package shared

// PaymentType is the type for payments
type PaymentType string

const (
	// PaymentTypeCash used for cash payments
	PaymentTypeCash PaymentType = "cash"
	// PaymentTypeCard used for any debit payments
	PaymentTypeCard PaymentType = "debit"
	// PaymentTypeCredit used for credit card payments
	PaymentTypeCredit PaymentType = "credit"
)

// Exists checks if the payment type presented is one of the existing in this file
func PaymentExists(paymentType PaymentType) bool {
	if paymentType == PaymentTypeCash {
		return true
	}

	if paymentType == PaymentTypeCard {
		return true
	}

	if paymentType == PaymentTypeCredit {
		return true
	}

	return false
}
