package logger

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/gofrs/uuid"
)

type logLevel string

const (
	infoLevel    logLevel = "info"
	warningLevel logLevel = "warning"
	errorLevel   logLevel = "error"
	fatalLevel   logLevel = "fatal"
)

func logEvent(originFile, originFunction, eventName string, level logLevel, properties map[string]interface{}) {
	output := map[string]interface{}{
		"id":         uuid.Must(uuid.NewV4()).String(),
		"event":      eventName,
		"level":      level,
		"file":       originFile,
		"function":   originFunction,
		"properties": properties,
		"time":       time.Now().Format(time.RFC3339),
	}

	jsonData, err := json.MarshalIndent(output, "", "  ")
	if err != nil {
		log.Println(output)
		log.Println("error marshaling data:", err)
		return
	}

	fmt.Println(string(jsonData))
}

func mapObjectsToProperties(objects []Object) map[string]interface{} {
	properties := map[string]interface{}{}

	for _, object := range objects {
		properties[object.LogName()] = object.LogProperties()
	}
	return properties
}

// Info logs a info event
func Info(originFile, originFunction, eventName string, objects ...Object) {
	logEvent(originFile, originFunction, eventName, infoLevel, mapObjectsToProperties(objects))
}

// Warning logs a warning event
func Warning(originFile, originFunction, eventName string, objects ...Object) {
	logEvent(originFile, originFunction, eventName, warningLevel, mapObjectsToProperties(objects))
}

// Error logs a error event
func Error(originFile, originFunction, eventName string, objects ...Object) {
	logEvent(originFile, originFunction, eventName, errorLevel, mapObjectsToProperties(objects))
}

// Fatal logs a fatal event
func Fatal(originFile, originFunction, eventName string, objects ...Object) {
	logEvent(originFile, originFunction, eventName, fatalLevel, mapObjectsToProperties(objects))
}

// CloseOrLog close a object reader
func CloseOrLog(originFile, originFunction string, closable io.Closer) {
	if err := closable.Close(); err != nil {
		Warning(originFile, originFunction, "closing_object_failed", ErrObject(err))
	}
}
