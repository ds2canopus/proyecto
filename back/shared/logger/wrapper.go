package logger

// Wrapper wraps any object as a loggable object
type Wrapper struct {
	Name   string
	Fields map[string]interface{}
}

// LogName returns the name of the object to log
func (w *Wrapper) LogName() string {
	return w.Name
}

// LogProperties returns the fields to log
func (w *Wrapper) LogProperties() map[string]interface{} {
	return w.Fields
}
