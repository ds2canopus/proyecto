package logger

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"runtime/debug"
	"time"
)

// Object represents a logger object
type Object interface {
	LogName() string
	LogProperties() map[string]interface{}
}

// ErrObject returns an Object containing error information
func ErrObject(err error) Object {
	return &Wrapper{
		Name: "error",
		Fields: map[string]interface{}{
			"s_err": err.Error(),
		},
	}
}

// PanicObject returns an Object containing panic information
func PanicObject(panic interface{}) Object {
	return &Wrapper{
		Name: "panic",
		Fields: map[string]interface{}{
			"s_message": fmt.Sprintf("%v", panic),
			"s_trace":   string(debug.Stack()),
		},
	}
}

// MapObject create object with data
func MapObject(logName string, data map[string]interface{}) Object {
	return &Wrapper{
		Name:   logName,
		Fields: data,
	}
}

// HTTPRequest returns an logger object given an http request
func HTTPRequest(request *http.Request) Object {
	return &Wrapper{
		Name: "http_request",
		Fields: map[string]interface{}{
			"s_method": request.Method,
			"s_host":   request.URL.Host,
			"s_path":   request.URL.Path,
			"s_query":  request.URL.RawQuery,
		},
	}
}

// HTTPResponse returns an logger object given an http response
func HTTPResponse(response *http.Response, timeTaken time.Duration) Object {
	fields := map[string]interface{}{
		"i_status":   response.StatusCode,
		"f_duration": timeTaken / time.Millisecond,
	}

	// error
	if response.StatusCode >= 400 {
		fields["s_body"] = getResponseBodyChunk(response)
	}

	return &Wrapper{
		Name:   "http_response",
		Fields: fields,
	}
}

func getResponseBodyChunk(response *http.Response) string {
	body, err := httputil.DumpResponse(response, true)
	if err == nil {
		maxBodySize := 256
		if len(body) < maxBodySize {
			maxBodySize = len(body)
		}
		return string(body[0:maxBodySize]) // we just get the first part of the response
	}

	return ""
}
