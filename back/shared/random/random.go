package random

import (
	"crypto/rand"
	"encoding/hex"
	math "math/rand"
	"strconv"
	"time"
)

const (
	_attemptsToReadRandomData = 3
)

func init() {
	math.Seed(time.Now().Unix())
}

// GenerateRandomData returns secure random data with the given size
func GenerateRandomData(bitSize int) []byte {
	buffer := make([]byte, bitSize)

	for i := 0; i < _attemptsToReadRandomData; i++ {
		_, err := rand.Read(buffer)
		if err == nil {
			break
		}
	}

	return buffer
}

// GenerateRandomHexString returns secure random hex string
func GenerateRandomHexString(bitSize int) string {
	return hex.EncodeToString(GenerateRandomData(bitSize))
}

// GenerateID generates a ID with the given prefix
func GenerateID(prefix string) string {
	return prefix + GenerateRandomHexString(16)
}

// Number generates a random  number
func Number() string {
	return strconv.Itoa(math.Int())
}

// RandomEntity returns a random entity
func RandomEntity() string {
	r := math.Float64()

	if r < 1.0/3.0 {
		return "Visa"
	}

	if r < 2.0/3.0 {
		return "Mastercard"
	}

	return "American Express"
}
