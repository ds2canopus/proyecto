package authentication

import (
	"errors"

	"golang.org/x/crypto/bcrypt"
)

var (
	// ErrInvalidPassword when a given password does not match a given hash
	ErrInvalidPassword = errors.New("invalid password")
)

// HashPassword given a unhashed password
func HashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		return "", err
	}

	return string(hash), nil
}

// CompareHash with a given plaintext password
func CompareHash(hash, password string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return ErrInvalidPassword
	}

	if err != nil {
		return err
	}

	return nil
}
