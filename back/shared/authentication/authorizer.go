package authentication

import (
	"errors"

	jwt "github.com/dgrijalva/jwt-go"
)

const signingKey = "canopus"

var (
	// ErrUnexpectedSign when the signing mehod is not HMAC
	ErrUnexpectedSign = errors.New("unexpected signing method")

	// ErrInvalidToken when the token parsing failed
	ErrInvalidToken = errors.New("invalid token")
)

// GenerateToken using HMAC signing method
func GenerateToken(claims map[string]interface{}) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims(claims))

	tokenString, err := token.SignedString([]byte(signingKey))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// ParseToken returns the claims of a valid token
func ParseToken(tokenString string) (map[string]interface{}, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, ErrUnexpectedSign
		}

		return []byte(signingKey), nil
	})

	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, ErrInvalidToken
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, ErrInvalidToken
	}

	return map[string]interface{}(claims), nil
}
