package authentication

import (
	"testing"

	"github.com/stretchr/testify/require"
	"golang.org/x/crypto/bcrypt"
)

func TestHashPassword(t *testing.T) {
	c := require.New(t)

	pass, err := HashPassword("password")
	c.Contains(pass, "$2a$04")
	c.Nil(err)
}

func TestCompareHash(t *testing.T) {
	c := require.New(t)

	err := CompareHash("$2a$04$cAfT6a.aJ4eSmuuj2gO2Euk/wm42nzBLOL/Pz9GRmOvuz16qYJzwy", "password")
	c.Nil(err)

	err = CompareHash("$2a$04$/Gmm29UWNVi3DraViJKrqursbWEMcI4EB6gP9ue3Ry4930lXDYFdc", "password")
	c.Equal(ErrInvalidPassword, err)

	err = CompareHash("", "password")
	c.Equal(bcrypt.ErrHashTooShort, err)
}
