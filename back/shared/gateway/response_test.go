package gateway

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

type mockWriter struct {
	header http.Header
	body   []byte
	status int
}

func (m *mockWriter) Header() http.Header {
	if m.header == nil {
		m.header = map[string][]string{}
	}

	return m.header
}

func (m *mockWriter) Write(body []byte) (int, error) {
	m.body = body
	return 0, nil
}

func (m *mockWriter) WriteHeader(statusCode int) {
	m.status = statusCode
}

func TestWithJSON(t *testing.T) {
	c := require.New(t)

	mock := &mockWriter{}
	response := &Response{mock}

	response.WithJSON(200, "body")
	c.Equal(`"body"`, string(mock.body))
	c.Equal(200, mock.status)

	response.WithJSON(200, make(chan int))
	c.Equal(`{"error":"something bad happend while writing the response"}`, string(mock.body))
	c.Equal(http.StatusInternalServerError, mock.status)
}
