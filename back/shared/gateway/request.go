package gateway

import (
	"errors"
	"net/http"

	"univalle/desarrollo/proyecto/back/shared/authentication"
)

var (
	// ErrEmptyAuthorizer when authorizer is empty
	ErrEmptyAuthorizer = errors.New("empty authorizer")

	// ErrInvalidAuthorizer when authorizer is empty
	ErrInvalidAuthorizer = errors.New("invalid authorizer")
)

// Request extension of the http.Request
type Request struct {
	*http.Request
	Level      string
	Email      string
	Authorizer map[string]interface{}
}

// Authorize the request given a level
func (r *Request) Authorize(level string) error {
	r.Authorizer = map[string]interface{}{}
	if level == "" {
		return nil
	}

	token := r.Header.Get("Authorizer")
	if token == "" {
		return ErrEmptyAuthorizer
	}

	claims, err := authentication.ParseToken(token)
	if err != nil {
		return err
	}

	lvl, ok := claims["level"].(string)
	if !ok || (level != lvl && level != "") {
		return ErrInvalidAuthorizer
	}

	email, ok := claims["email"].(string)
	if !ok || email == "" {
		return ErrInvalidAuthorizer
	}

	r.Level = lvl
	r.Email = email
	r.Authorizer = claims
	return nil
}
