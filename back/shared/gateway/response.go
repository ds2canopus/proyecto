package gateway

import (
	"encoding/json"
	"net/http"

	"univalle/desarrollo/proyecto/back/shared/logger"
)

// Response extension of the http.ResponseWriter
type Response struct {
	http.ResponseWriter
}

// WithJSON responds with a JSON content-type
func (r Response) WithJSON(statusCode int, v interface{}) {
	r.Header().Set("Content-Type", "application/json")
	r.Header().Set("Access-Control-Allow-Origin", "*")

	data, err := json.Marshal(v)
	if err == nil {
		r.WriteHeader(statusCode)
		_, _ = r.Write(data)
		return
	}

	logger.Error("shared/gateway/response.go", "WithJSON", "marshal_response_failed", logger.ErrObject(err))

	r.WriteHeader(http.StatusInternalServerError)
	_, _ = r.Write([]byte(`{"error":"something bad happend trying to marshal the response"}`))
}
