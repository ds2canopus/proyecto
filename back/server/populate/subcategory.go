package main

type subcategory struct {
	ID         string `json:"id"`
	CategoryID string `json:"category_id"`
	Name       string `json:"name"`
	Image      string `json:"image"`
	imagePath  string
	products   []product
}

var (
	bonsai = subcategory{
		Name:      "Bonsái",
		imagePath: "./imagenes/arboles/bonsai/Bonsái Guayacán.jpg",
		products: []product{
			bonsaiGuayacan,
			bonsaiJaboticaba,
			bonsaiPinoAzul,
			bonsaiPinoEstrella,
			bonsaiPinoLibroGrande,
			bonsaiPinoSemidorado,
			bonsaiPinoTunya,
			bonsaiPortulacaria,
		},
	}

	climaCalido = subcategory{
		Name:      "Clima cálido",
		imagePath: "./imagenes/arboles/clima_calido/Árbol Acacia Ferrugínea.jpg",
		products: []product{
			arbolAcaciaAmarilla,
			arbolAcaciaFerruginea,
			arbolAcaciaRoja,
			arbolCastanoIndias,
			arbolGalanNoche,
			arbolGualanday,
			arbolGuayacanAmarillo,
			arbolGuayacanAzul,
			arbolVelero,
		},
	}

	climaFrio = subcategory{
		Name:      "Clima frio",
		imagePath: "./imagenes/arboles/clima_frio/Árbol Amarrabollo.jpg",
		products: []product{
			arbolAliso,
			arbolAmarrabolloRosado,
			arbolAmarrabollo,
			arbolCalistemoBlanco,
			arbolCalistemoRojo,
			arbolCanelo,
			arbolCariseco,
			arbolEucaliptoArcoiris,
			arbolPinoPatula,
			arbolSieteCuerosBlanco,
			arbolYarumoBlanco,
		},
	}
)

var (
	controlPlagas = subcategory{
		Name:      "Control de plagas",
		imagePath: "./imagenes/jardineria/control de plagas/Diatomeas.jpg",
		products: []product{
			diatomeas,
			polillas,
			palomilla,
			hongos,
			babosas,
			acaros,
			brillaHojas,
		},
	}

	herramientas = subcategory{
		Name:      "Herramientas",
		imagePath: "./imagenes/jardineria/herramientas/Juego Jardín 3 Piezas Madera.jpg",
		products: []product{
			cernidorTierra,
			juegoJardin,
			kitGerminación,
			pistolaPlástica,
			regadera,
			setHerramientas,
			podadora,
		},
	}

	suelos = subcategory{
		Name:      "Suelos y plagas",
		imagePath: "./imagenes/jardineria/suelos y sustratos/Sustrato de perlita.jpg",
		products: []product{
			lombricultivos,
			sustratoArlita,
			sustratoPerlita,
			sustratoSuculentas,
			sustratoOrquideas,
			sustratoInterior,
			tacosPropagación,
		},
	}
)

var (
	barro = subcategory{
		Name:      "Barro",
		imagePath: "./imagenes/macetas/barro/Maceta Semillero Tipo Fuente #2.jpg",
		products: []product{
			cantaro,
			barroTuboVeteado,
			barroTresPatas,
			barroColgantePared,
			barroMoyo,
			barroSemillero,
			barroCampana,
		},
	}

	ceramica = subcategory{
		Name:      "Ceramica",
		imagePath: "./imagenes/macetas/ceramica/Maceta Cerámica Dinosaurio Estegosaurio de Mesa.jpg",
		products: []product{
			ceramicaCalavera,
			ceramicaBuda,
			ceramicaDinosaurio,
			ceramicaGota,
			ceramicaPerro,
			barroCracelada,
			barroEsfera,
		},
	}

	plastico = subcategory{
		Name:      "Plastico",
		imagePath: "./imagenes/macetas/plastico/Jardinera Vertical de 5 niveles.jpg",
		products: []product{
			plasticoColgante,
			plasticoRoma,
			plasticoVertical,
			plasticoMaceta,
			plasticoMimbre,
			plasticoTornado,
			plasticoZombies,
		},
	}

	aromaticas = subcategory{
		Name:      "Aromáticas",
		imagePath: "./imagenes/plantas/aromaticas/Planta Toronjil.jpg",
		products: []product{
			kitPlantasAromaticas,
			plantaAlbahacaMorada,
			plantaMenta,
			plantaOreganoMedicinal,
			plantaRuda,
			plantaTomillo,
			plantaToronjil,
		},
	}

	flores = subcategory{
		Name:      "Flores",
		imagePath: "./imagenes/plantas/flores/Planta Anturio Cirano Morado.jpg",
		products: []product{
			florCera,
			ciranoMorado,
		},
	}

	suculentas = subcategory{
		Name:      "Suculentas",
		imagePath: "./imagenes/plantas/suculentas/Planta Jade.jpg",
		products: []product{
			plantaJade,
		},
	}

	sAromaticas = subcategory{
		Name:      "Aromaticas",
		imagePath: "./imagenes/semillas/aromaticas/Semillas Albahaca Hojas de Lechuga.jpg",
		products: []product{
			semillasAlbahaca,
		},
	}

	sFlores = subcategory{
		Name:      "Flores",
		imagePath: "./imagenes/semillas/flores/Semillas Aster Mezcla de Colores.jpg",
		products: []product{
			semillasAster,
		},
	}

	hortalizas = subcategory{
		Name:      "hortalizas",
		imagePath: "./imagenes/semillas/hortalizas/Semilla Cebolla Puerro.jpg",
		products: []product{
			semillaCebolla,
		},
	}
)
