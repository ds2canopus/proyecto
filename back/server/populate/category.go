package main

type category struct {
	ID          string `json:"category_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Image       string `json:"image"`
	imagePath   string
	subs        []subcategory
}

var (
	arboles = category{
		Name:        "Árboles",
		Description: "Venta de árboles",
		imagePath:   "./imagenes/arboles/bonsai/Bonsái Guayacán.jpg",
		subs: []subcategory{
			bonsai,
			climaCalido,
			climaFrio,
		},
	}

	jardineria = category{
		Name:        "Jardinería",
		Description: "Productos de jardinería",
		imagePath:   "./imagenes/jardineria/control de plagas/Diatomeas.jpg",
		subs: []subcategory{
			controlPlagas,
			herramientas,
			suelos,
		},
	}

	macetas = category{
		Name:        "Macetas",
		Description: "Venta de macetas",
		imagePath:   "./imagenes/macetas/plastico/Jardinera Vertical de 5 niveles.jpg",
		subs: []subcategory{
			barro,
			ceramica,
			plastico,
		},
	}

	plantas = category{
		Name:        "Plantas",
		Description: "Venta de plantas",
		imagePath:   "./imagenes/plantas/suculentas/Planta Jade.jpg",
		subs: []subcategory{
			aromaticas,
			flores,
			suculentas,
		},
	}

	semillas = category{
		Name:        "Semillas",
		Description: "Venta de semillas",
		imagePath:   "./imagenes/semillas/aromaticas/Semillas Albahaca Hojas de Lechuga.jpg",
		subs: []subcategory{
			sAromaticas,
			sFlores,
			hortalizas,
		},
	}
)
