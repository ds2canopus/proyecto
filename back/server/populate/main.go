package main

import (
	"bufio"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

var (
	categoryURL    = "http://localhost:8080/category"
	subcategoryURL = "http://localhost:8080/subcategory"
	productURL     = "http://localhost:8080/product"

	n = 0
)

func doRequest(url string, payload interface{}) string {
	body, err := json.Marshal(payload)
	if err != nil {
		panic(err)
	}

	req, err := http.NewRequest("POST", url, strings.NewReader(string(body)))
	if err != nil {
		panic(err)
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorizer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImVtYWlsIiwibGV2ZWwiOiJhZG1pbiJ9.2Gdvo48EK346WtknuyJVAQCYpIDQx7uYH36I-bgGtzE")
	req.Header.Add("User-Agent", "PostmanRuntime/7.16.3")
	req.Header.Add("Accept", "*/*")
	req.Header.Add("Cache-Control", "no-cache")
	req.Header.Add("Postman-Token", "55e4d224-ec7a-4e4a-bf2c-3acc94d202c3,1c2aeee3-414c-4776-953b-c4e6e86fc825")
	req.Header.Add("Host", "localhost:8080")
	req.Header.Add("Accept-Encoding", "gzip, deflate")
	req.Header.Add("Content-Length", "93")
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("cache-control", "no-cache")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	response := map[string]string{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		panic(err)
	}

	if m, ok := response["error"]; ok {
		panic(m)
	}

	return response["id"]
}

func readImage(path string) string {
	imgFile, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer imgFile.Close()

	fInfo, _ := imgFile.Stat()
	var size int64 = fInfo.Size()
	buf := make([]byte, size)

	fReader := bufio.NewReader(imgFile)
	_, err = fReader.Read(buf)
	if err != nil {
		panic(err)
	}

	img := "data:image/png;base64," + base64.StdEncoding.EncodeToString(buf)
	return img
}

func saveCategory(cat category) {
	cat.Image = readImage(cat.imagePath)
	catID := doRequest(categoryURL, cat)

	fmt.Println(n)
	fmt.Println(catID)
	n++

	for i, sub := range cat.subs {
		sub.CategoryID = catID
		sub.Image = readImage(sub.imagePath)
		subID := doRequest(subcategoryURL, sub)

		fmt.Println("	", i)
		fmt.Println(subID)

		for j, prd := range sub.products {
			prd.SubcategoryID = subID
			prd.Image = readImage(prd.imagePath)
			_ = doRequest(productURL, prd)

			fmt.Println("		", j)
		}
	}
}

func main() {
	saveCategory(arboles)
	saveCategory(jardineria)
	saveCategory(macetas)
	saveCategory(plantas)
	saveCategory(semillas)
}
