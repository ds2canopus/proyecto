package main

type product struct {
	ID            string  `json:"id"`
	SubcategoryID string  `json:"subcategory_id"`
	Name          string  `json:"name"`
	Price         float64 `json:"price"`
	Description   string  `json:"description"`
	Quantity      int     `json:"quantity"`
	Image         string  `json:"image"`
	IVA           float64 `json:"iva"`
	imagePath     string
}

var (
	bonsaiGuayacan = product{
		Name:        "Bonsái Guayacán",
		Price:       100,
		Description: "Bonsái Guayacán",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/bonsai/Bonsái Guayacán.jpg",
		IVA:         19,
	}

	bonsaiJaboticaba = product{
		Name:        "Bonsái Jaboticaba",
		Price:       100,
		Description: "Bonsái Jaboticaba",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/bonsai/Bonsái Jaboticaba.jpg",
		IVA:         19,
	}

	bonsaiPinoAzul = product{
		Name:        "Bonsái Pino Azul",
		Price:       100,
		Description: "Bonsái Pino Azul",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/bonsai/Bonsái Pino Azul.jpg",
		IVA:         19,
	}

	bonsaiPinoEstrella = product{
		Name:        "Bonsái Pino Estrella",
		Price:       100,
		Description: "Bonsái Pino Estrella",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/bonsai/Bonsái Pino Estrella.jpg",
		IVA:         19,
	}

	bonsaiPinoLibroGrande = product{
		Name:        "Bonsái Pino Libro Grande",
		Price:       100,
		Description: "Bonsái Pino Libro Grande",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/bonsai/Bonsái Pino Libro Grande.jpg",
		IVA:         19,
	}

	bonsaiPinoSemidorado = product{
		Name:        "Bonsái Pino Semidorado",
		Price:       100,
		Description: "Bonsái Pino Semidorado",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/bonsai/Bonsái Pino Semidorado.jpg",
		IVA:         19,
	}

	bonsaiPinoTunya = product{
		Name:        "Bonsái Pino Tunya",
		Price:       100,
		Description: "Bonsái Pino Tunya",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/bonsai/Bonsái Pino Tunya.jpg",
		IVA:         19,
	}

	bonsaiPortulacaria = product{
		Name:        "Bonsái Portulacaria afra",
		Price:       100,
		Description: "Bonsái Portulacaria afra en Maceta de Cemento",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/bonsai/Bonsái Portulacaria afra en Maceta de Cemento.jpg",
		IVA:         19,
	}
)

var (
	arbolAcaciaAmarilla = product{
		Name:        "Árbol Acacia Amarilla",
		Price:       100,
		Description: "Árbol Acacia Amarilla",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_calido/Árbol Acacia Amarilla.jpg",
		IVA:         19,
	}

	arbolAcaciaFerruginea = product{
		Name:        "Árbol Acacia Ferrugínea",
		Price:       100,
		Description: "Árbol Acacia Ferrugínea",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_calido/Árbol Acacia Ferrugínea.jpg",
		IVA:         19,
	}

	arbolAcaciaRoja = product{
		Name:        "Árbol Acacia Roja",
		Price:       100,
		Description: "Árbol Acacia Roja",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_calido/Árbol Acacia Roja.jpg",
		IVA:         19,
	}

	arbolCastanoIndias = product{
		Name:        "Árbol Castaño de Indias",
		Price:       100,
		Description: "Árbol Castaño de Indias",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_calido/Árbol Castaño de Indias.jpg",
		IVA:         19,
	}

	arbolGalanNoche = product{
		Name:        "Árbol Galán de Noche",
		Price:       100,
		Description: "Árbol Galán de Noche",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_calido/Árbol Galán de Noche.jpg",
		IVA:         19,
	}

	arbolGualanday = product{
		Name:        "Árbol Gualanday",
		Price:       100,
		Description: "Árbol Acacia Amarilla",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_calido/Árbol Gualanday.jpg",
		IVA:         19,
	}

	arbolGuayacanAmarillo = product{
		Name:        "Árbol Guayacán Amarillo",
		Price:       100,
		Description: "Árbol Guayacán Amarillo",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_calido/Árbol Guayacán Amarillo.jpg",
		IVA:         19,
	}

	arbolGuayacanAzul = product{
		Name:        "Árbol Guayacán Azul",
		Price:       100,
		Description: "Árbol Guayacán Azul",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_calido/Árbol Guayacán Azul.jpg",
		IVA:         19,
	}

	arbolVelero = product{
		Name:        "Árbol Velero",
		Price:       100,
		Description: "Árbol Velero",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_calido/Árbol Velero.jpg",
		IVA:         19,
	}
)

var (
	arbolAliso = product{
		Name:        "Árbol Aliso",
		Price:       100,
		Description: "Árbol Aliso",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Aliso.jpg",
		IVA:         19,
	}

	arbolAmarrabolloRosado = product{
		Name:        "Árbol Amarrabollo Rosado",
		Price:       100,
		Description: "Árbol Amarrabollo Rosado",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Amarrabollo Rosado.jpg",
		IVA:         19,
	}

	arbolAmarrabollo = product{
		Name:        "Árbol Amarrabollo",
		Price:       100,
		Description: "Árbol Amarrabollo",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Amarrabollo.jpg",
		IVA:         19,
	}

	arbolCalistemoBlanco = product{
		Name:        "Árbol Calistemo Blanco",
		Price:       100,
		Description: "Árbol Calistemo Blanco",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Calistemo Blanco.jpg",
		IVA:         19,
	}

	arbolCalistemoRojo = product{
		Name:        "Árbol Calistemo Rojo",
		Price:       100,
		Description: "Árbol Calistemo Rojo",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Calistemo Rojo.jpg",
		IVA:         19,
	}

	arbolCanelo = product{
		Name:        "Árbol Canelo",
		Price:       100,
		Description: "Árbol Canelo",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Canelo.jpg",
		IVA:         19,
	}

	arbolCariseco = product{
		Name:        "Árbol Cariseco",
		Price:       100,
		Description: "Árbol Cariseco",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Cariseco.jpg",
		IVA:         19,
	}

	arbolEucaliptoArcoiris = product{
		Name:        "Árbol Eucalipto Arcoiris",
		Price:       100,
		Description: "Árbol Eucalipto Arcoiris",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Eucalipto Arcoiris.jpg",
		IVA:         19,
	}

	arbolPinoPatula = product{
		Name:        "Árbol Pino Patula",
		Price:       100,
		Description: "Árbol Pino Patula",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Pino Patula.jpg",
		IVA:         19,
	}

	arbolSieteCuerosBlanco = product{
		Name:        "Árbol Siete Cueros Blanco",
		Price:       100,
		Description: "Árbol Siete Cueros Blanco",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Siete Cueros Blanco.jpg",
		IVA:         19,
	}

	arbolYarumoBlanco = product{
		Name:        "Árbol Yarumo Blanco",
		Price:       100,
		Description: "Árbol Yarumo Blanco",
		Quantity:    10,
		imagePath:   "./imagenes/arboles/clima_frio/Árbol Yarumo Blanco.jpg",
		IVA:         19,
	}
)

var (
	diatomeas = product{
		Name:        "Diatomeas",
		Price:       100,
		Description: "Diatomeas",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/control de plagas/Diatomeas.jpg",
		IVA:         19,
	}

	polillas = product{
		Name:        "Control polilla",
		Price:       100,
		Description: "Para control de polillas",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/control de plagas/Control Polilla.jpg",
		IVA:         19,
	}

	palomilla = product{
		Name:        "Control palomilla",
		Price:       100,
		Description: "Para control de palomilla",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/control de plagas/Control Palomilla.jpg",
		IVA:         19,
	}

	hongos = product{
		Name:        "Control hongos",
		Price:       100,
		Description: "Para control de hongos",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/control de plagas/Control Hongos.jpg",
		IVA:         19,
	}

	babosas = product{
		Name:        "Control babosas",
		Price:       100,
		Description: "Para control de babosas",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/control de plagas/Control Babosas.jpg",
		IVA:         19,
	}

	acaros = product{
		Name:        "Control acaros",
		Price:       100,
		Description: "Para control de acaros",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/control de plagas/Control Ácaros.jpg",
		IVA:         19,
	}

	brillaHojas = product{
		Name:        "Brilla hojas ecobrillo",
		Price:       100,
		Description: "Brilla hojas ecobrillo",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/control de plagas/Brilla Hojas Ecobrillo.jpg",
		IVA:         19,
	}
)

var (
	cernidorTierra = product{
		Name:        "Cernidor de tierra o Zaranda",
		Price:       100,
		Description: "Cernidor de tierra o Zaranda",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/herramientas/Cernidor de tierra o Zaranda.jpg",
		IVA:         19,
	}

	juegoJardin = product{
		Name:        "Juego Jardín",
		Price:       100,
		Description: "3 Piezas Madera",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/herramientas/Juego Jardín 3 Piezas Madera.jpg",
		IVA:         19,
	}

	kitGerminación = product{
		Name:        "Kit Bandeja de Germinación",
		Price:       100,
		Description: "12 Cavidades",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/herramientas/Kit Bandeja de Germinación de 12 Cavidades.jpg",
		IVA:         19,
	}

	pistolaPlástica = product{
		Name:        "Pistola Plástica 5 Funciones",
		Price:       100,
		Description: "5 Funciones",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/herramientas/Pistola Plástica 5 Funciones.jpg",
		IVA:         19,
	}

	regadera = product{
		Name:        "Regadera",
		Price:       100,
		Description: "5 lts",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/herramientas/Regadera x 5 lts.jpg",
		IVA:         19,
	}

	setHerramientas = product{
		Name:        "Set de Herramientas",
		Price:       100,
		Description: "Trasplante y Siembra de Plántulas",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/herramientas/Set de Herramientas Trasplante y Siembra de Plántulas.jpg",
		IVA:         19,
	}

	podadora = product{
		Name:        "Tijera Podadora Para Flores",
		Price:       100,
		Description: "Tijera Podadora Para Flores",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/herramientas/Tijera Podadora Para Flores.jpg",
		IVA:         19,
	}
)

var (
	lombricultivos = product{
		Name:        "Mezcla especial para lombricultivos",
		Price:       100,
		Description: "Mezcla especial para lombricultivos",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/suelos y sustratos/Mezcla especial para lombricultivos.jpg",
		IVA:         19,
	}

	sustratoArlita = product{
		Name:        "Sustrato Arlita",
		Price:       100,
		Description: "Sustrato Arlita",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/suelos y sustratos/Sustrato Arlita.jpg",
		IVA:         19,
	}

	sustratoPerlita = product{
		Name:        "Sustrato de perlita",
		Price:       100,
		Description: "Sustrato de perlita",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/suelos y sustratos/Sustrato de perlita.jpg",
		IVA:         19,
	}

	sustratoSuculentas = product{
		Name:        "Sustrato para cactus y suculentas",
		Price:       100,
		Description: "Sustrato para cactus y suculentas",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/suelos y sustratos/Sustrato para cactus y suculentas.jpg",
		IVA:         19,
	}

	sustratoOrquideas = product{
		Name:        "Sustrato para orquídeas",
		Price:       100,
		Description: "Sustrato para orquídeas",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/suelos y sustratos/Sustrato para orquídeas.jpg",
		IVA:         19,
	}

	sustratoInterior = product{
		Name:        "Sustrato para plantas de interior",
		Price:       100,
		Description: "Sustrato para plantas de interior",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/suelos y sustratos/Sustrato para plantas de interior.jpg",
		IVA:         19,
	}

	tacosPropagación = product{
		Name:        "Tacos de propagación",
		Price:       100,
		Description: "Tacos de propagación",
		Quantity:    10,
		imagePath:   "./imagenes/jardineria/suelos y sustratos/Tacos de propagación.jpg",
		IVA:         19,
	}
)

var (
	cantaro = product{
		Name:        "Cántaro de Barro",
		Price:       100,
		Description: "Cántaro de Barro",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/barro/Cántaro de Barro #1.jpg",
		IVA:         19,
	}

	barroTuboVeteado = product{
		Name:        "Jarrón Barro Tubo Veteado",
		Price:       100,
		Description: "Jarrón Barro Tubo Veteado",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/barro/Jarrón Barro Tubo Veteado #1.jpg",
		IVA:         19,
	}

	barroTresPatas = product{
		Name:        "Maceta Barro De Tres Patas #2",
		Price:       100,
		Description: "Maceta Barro De Tres Patas #2",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/barro/Maceta Barro De Tres Patas #2.jpg",
		IVA:         19,
	}

	barroColgantePared = product{
		Name:        "Maceta Colgante de Pared",
		Price:       100,
		Description: "Maceta Colgante de Pared",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/barro/Maceta Colgante de Pared #4.jpg",
		IVA:         19,
	}

	barroMoyo = product{
		Name:        "Maceta Jarrón Moyo Veteado",
		Price:       100,
		Description: "Maceta Jarrón Moyo Veteado",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/barro/Maceta Jarrón Moyo Veteado #1.jpg",
		IVA:         19,
	}

	barroSemillero = product{
		Name:        "Maceta Semillero Tipo Fuente",
		Price:       100,
		Description: "Maceta Semillero Tipo Fuente",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/barro/Maceta Semillero Tipo Fuente #2.jpg",
		IVA:         19,
	}

	barroCampana = product{
		Name:        "Maceta Tipo Campana",
		Price:       100,
		Description: "Maceta Tipo Campana",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/barro/Maceta Tipo Campana #3.jpg",
		IVA:         19,
	}
)

var (
	ceramicaCalavera = product{
		Name:        "Maceta Calavera de Mesa",
		Price:       100,
		Description: "Maceta Calavera de Mesa",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/ceramica/Maceta Calavera de Mesa.jpg",
		IVA:         19,
	}

	ceramicaBuda = product{
		Name:        "Maceta Buda",
		Price:       100,
		Description: "Maceta Cerámica Buda",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/ceramica/Maceta Cerámica Buda.jpg",
		IVA:         19,
	}

	ceramicaDinosaurio = product{
		Name:        "Maceta Dinosaurio",
		Price:       100,
		Description: "Maceta Cerámica Dinosaurio Estegosaurio de Mesa",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/ceramica/Maceta Cerámica Dinosaurio Estegosaurio de Mesa.jpg",
		IVA:         19,
	}

	ceramicaGota = product{
		Name:        "Maceta Gota",
		Price:       100,
		Description: "Maceta Cerámica Gota Colgante",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/ceramica/Maceta Cerámica Gota Colgante.jpg",
		IVA:         19,
	}

	ceramicaPerro = product{
		Name:        "Maceta Perro",
		Price:       100,
		Description: "Maceta Cerámica Perro Sentado",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/ceramica/Maceta Cerámica Perro Sentado.jpg",
		IVA:         19,
	}

	barroCracelada = product{
		Name:        "Maceta Craquelada",
		Price:       100,
		Description: "Maceta de Cerámica Craquelada",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/ceramica/Maceta de Cerámica Craquelada.jpg",
		IVA:         19,
	}

	barroEsfera = product{
		Name:        "Maceta Esfera Craquelada",
		Price:       100,
		Description: "Maceta Esfera Cerámica Grande Craquelada",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/ceramica/Maceta Esfera Cerámica Grande Craquelada.jpg",
		IVA:         19,
	}
)

var (
	plasticoColgante = product{
		Name:        "Canasta colgante",
		Price:       100,
		Description: "25 cm",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/plastico/Canasta colgante 25 cm.jpg",
		IVA:         19,
	}

	plasticoRoma = product{
		Name:        "Jardinera Roma Balcón",
		Price:       100,
		Description: "Jardinera Roma Balcón",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/plastico/Jardinera Roma Balcón.jpg",
		IVA:         19,
	}

	plasticoVertical = product{
		Name:        "Jardinera Vertical",
		Price:       100,
		Description: "5 niveles",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/plastico/Jardinera Vertical de 5 niveles.jpg",
		IVA:         19,
	}

	plasticoMaceta = product{
		Name:        "Maceta",
		Price:       100,
		Description: "8 cm",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/plastico/Maceta 8 cm.jpg",
		IVA:         19,
	}

	plasticoMimbre = product{
		Name:        "Maceta Mimbre",
		Price:       100,
		Description: "16 cm Café",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/plastico/Maceta Mimbre 16 cm Café.jpg",
		IVA:         19,
	}

	plasticoTornado = product{
		Name:        "Maceta Tornado",
		Price:       100,
		Description: "30 cm",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/plastico/Maceta Tornado 30 cm.jpg",
		IVA:         19,
	}

	plasticoZombies = product{
		Name:        "Maceta Zombies",
		Price:       100,
		Description: "9 cm",
		Quantity:    10,
		imagePath:   "./imagenes/macetas/plastico/Maceta Zombies Quadria 9 cm.jpg",
		IVA:         19,
	}
)

var (
	kitPlantasAromaticas = product{
		Name:        "Kit Plantas Aromáticas",
		Price:       100,
		Description: "Kit Plantas Aromáticas",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/aromaticas/Kit Plantas Aromáticas.jpg",
		IVA:         19,
	}

	plantaAlbahacaMorada = product{
		Name:        "Planta Albahaca Morada",
		Price:       100,
		Description: "Planta Albahaca Morada",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/aromaticas/Planta Albahaca Morada.jpg",
		IVA:         19,
	}

	plantaMenta = product{
		Name:        "Planta Menta",
		Price:       100,
		Description: "Planta Menta",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/aromaticas/Planta Menta.jpg",
		IVA:         19,
	}

	plantaOreganoMedicinal = product{
		Name:        "Planta Oregano Medicinal",
		Price:       100,
		Description: "Planta Oregano Medicinal",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/aromaticas/Planta Oregano Medicinal.jpg",
		IVA:         19,
	}

	plantaRuda = product{
		Name:        "Planta Ruda",
		Price:       100,
		Description: "Planta Ruda",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/aromaticas/Planta Ruda.jpg",
		IVA:         19,
	}

	plantaTomillo = product{
		Name:        "Planta Tomillo",
		Price:       100,
		Description: "Planta Tomillo",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/aromaticas/Planta Tomillo.jpg",
		IVA:         19,
	}

	plantaToronjil = product{
		Name:        "Planta Toronjil",
		Price:       100,
		Description: "Planta Toronjil",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/aromaticas/Planta Toronjil.jpg",
		IVA:         19,
	}
)

var (
	florCera = product{
		Name:        "Esquejes de la Planta Flor de Cera Hoya",
		Price:       100,
		Description: "Esquejes de la Planta Flor de Cera Hoya",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/flores/Esquejes de la Planta Flor de Cera Hoya.jpg",
		IVA:         19,
	}

	ciranoMorado = product{
		Name:        "Planta Anturio Cirano Morado",
		Price:       100,
		Description: "Planta Anturio Cirano Morado",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/flores/Planta Anturio Cirano Morado.jpg",
		IVA:         19,
	}
)

var (
	plantaJade = product{
		Name:        "Planta Jade",
		Price:       100,
		Description: "Planta Jade",
		Quantity:    10,
		imagePath:   "./imagenes/plantas/suculentas/Planta Jade.jpg",
		IVA:         19,
	}
)

var (
	semillasAlbahaca = product{
		Name:        "Semillas Albahaca",
		Price:       100,
		Description: "Semillas Albahaca Hojas de Lechuga",
		Quantity:    10,
		imagePath:   "./imagenes/semillas/aromaticas/Semillas Albahaca Hojas de Lechuga.jpg",
		IVA:         19,
	}
)

var (
	semillasAster = product{
		Name:        "CampSemillas Aster Mezcla de Coloresana",
		Price:       100,
		Description: "Semillas Aster Mezcla de Colores",
		Quantity:    10,
		imagePath:   "./imagenes/semillas/flores/Semillas Aster Mezcla de Colores.jpg",
		IVA:         19,
	}
)

var (
	semillaCebolla = product{
		Name:        "Semilla Cebolla Puerro",
		Price:       100,
		Description: "Semilla Cebolla Puerro",
		Quantity:    10,
		imagePath:   "./imagenes/semillas/hortalizas/Semilla Cebolla Puerro.jpg",
		IVA:         19,
	}
)
