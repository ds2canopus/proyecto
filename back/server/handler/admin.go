package handler

import (
	"encoding/json"
	"errors"
	"net/http"

	"univalle/desarrollo/proyecto/back/models/admin"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
)

func loginAdmin(res *gateway.Response, req *gateway.Request) {
	credentials := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&credentials)
	if err != nil {
		logger.Error("server/handler/admin.go", "loginAdmin", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	token, err := admin.Login(credentials["email"], credentials["password"])
	if err != nil {
		logger.Error("server/handler/admin.go", "loginAdmin", "admin_login_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, map[string]string{"token": token})
}

func createAdmin(res *gateway.Response, req *gateway.Request) {
	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/admin.go", "createAdmin", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	adm, err := admin.New(data["email"], data["name"], data["password"])
	if err != nil {
		logger.Error("server/handler/admin.go", "createAdmin", "new_admin_error", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	err = adm.Store()
	if err != nil {
		logger.Error("server/handler/admin.go", "createAdmin", "store_admin_error", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, map[string]string{"status": "ok"})
}

func loadAdmin(res *gateway.Response, req *gateway.Request) {
	email, ok := req.URL.Query()["email"]

	if !ok || len(email) <= 0 {
		logger.Error("server/handler/admin.go", "loadAdmin", "missing_email_parameter", logger.ErrObject(errors.New("missing email parameter")))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": "missing email parameter"})
		return
	}

	adm, err := admin.Load(email[0])
	if err != nil {
		logger.Error("server/handler/admin.go", "loadAdmin", "load_admin_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, adm)
}

func updateAdmin(res *gateway.Response, req *gateway.Request) {
	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/admin.go", "updateAdmin", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	adm, err := admin.Load(data["email"])
	if err != nil {
		logger.Error("server/handler/admin.go", "updateAdmin", "load_admin_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = adm.Update(data["name"], data["password"])
	if err != nil {
		logger.Error("server/handler/admin.go", "updateAdmin", "update_admin_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, map[string]string{"status": "ok"})
}

func deleteAdmin(res *gateway.Response, req *gateway.Request) {
	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/admin.go", "deleteAdmin", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	ownerEmail, ok := req.Authorizer["email"].(string)
	if !ok {
		logger.Error("server/handler/admin.go", "deleteAdmin", "email_authorizer_cast_failed")
		res.WithJSON(401, map[string]string{"error": ErrUnauthorizedRequest.Error()})
		return
	}

	if data["email"] == ownerEmail {
		logger.Error("server/handler/admin.go", "deleteAdmin", "unauthorized_delete")
		res.WithJSON(401, map[string]string{"error": ErrUnauthorizedRequest.Error()})
		return
	}

	err = admin.Delete(data["email"])
	if err != nil {
		logger.Error("server/handler/admin.go", "deleteAdmin", "delete_admin_failed")
		res.WithJSON(401, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, map[string]string{"status": "ok"})
}

func listAdmins(res *gateway.Response, req *gateway.Request) {
	admins, err := admin.List()
	if err != nil {
		logger.Error("server/handler/admin.go", "listAdmins", "list_admins_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, admins)
}
