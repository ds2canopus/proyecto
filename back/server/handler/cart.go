package handler

import (
	"encoding/json"
	"net/http"
	"univalle/desarrollo/proyecto/back/models/cart"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
)

func storeCart(res *gateway.Response, req *gateway.Request) {
	crt := &cart.Cart{UserID: req.Email}

	err := json.NewDecoder(req.Body).Decode(crt)
	if err != nil {
		logger.Error("server/handler/cart.go", "createCart", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = crt.Store()
	if err != nil {
		logger.Error("server/handler/cart.go", "createCart", "store_cart_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusCreated, map[string]string{"status": "ok"})
}

func listCart(res *gateway.Response, req *gateway.Request) {
	carts, err := cart.List(req.Email)
	if err != nil {
		logger.Error("server/handler/cart.go", "listCart", "list_cart_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, carts)
}

func deleteCart(res *gateway.Response, req *gateway.Request) {
	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/cart.go", "deleteCart", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = cart.Delete(req.Email, data["product_id"])
	if err != nil {
		logger.Error("server/handler/cart.go", "deleteCart", "delete_cart_failed")
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}

func clearCart(res *gateway.Response, req *gateway.Request) {
	err := cart.Clear(req.Email)
	if err != nil {
		logger.Error("server/handler/cart.go", "clearCart", "clear_cart_failed")
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}
