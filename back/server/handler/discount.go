package handler

import (
	"encoding/json"
	"net/http"
	"univalle/desarrollo/proyecto/back/models/discount"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
)

func createDiscount(res *gateway.Response, req *gateway.Request) {
	dis := &discount.Discount{}

	err := json.NewDecoder(req.Body).Decode(dis)
	if err != nil {
		logger.Error("server/handler/discount.go", "createDiscount", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = dis.Store()
	if err != nil {
		logger.Error("server/handler/discount.go", "createDiscount", "store_discount_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusCreated, map[string]string{"status": "ok"})
}

func deleteDiscount(res *gateway.Response, req *gateway.Request) {
	dis := &discount.Discount{}

	err := json.NewDecoder(req.Body).Decode(dis)
	if err != nil {
		logger.Error("server/handler/discount.go", "deleteDiscount", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = dis.Delete()
	if err != nil {
		logger.Error("server/handler/discount.go", "deleteDiscount", "delete_discount_failed")
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}

func listDiscounts(res *gateway.Response, req *gateway.Request) {
	discounts, err := discount.ListAll()
	if err != nil {
		logger.Error("server/handler/discount.go", "listDiscounts", "list_discounts_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, discounts)
}

func listDiscountsUser(res *gateway.Response, req *gateway.Request) {
	userID, _ := req.URL.Query()["user_id"]

	discounts, err := discount.ListByUser(userID[0])
	if err != nil {
		logger.Error("server/handler/discount.go", "listDiscounts", "list_discounts_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, discounts)
}
