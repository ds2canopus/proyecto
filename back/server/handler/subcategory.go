package handler

import (
	"encoding/json"
	"net/http"
	"univalle/desarrollo/proyecto/back/models/subcategory"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
)

func createSubcategory(res *gateway.Response, req *gateway.Request) {
	sub := &subcategory.Subcategory{}

	err := json.NewDecoder(req.Body).Decode(sub)
	if err != nil {
		logger.Error("server/handler/subcategory.go", "createSubategory", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = sub.Store()
	if err != nil {
		logger.Error("server/handler/subcategory.go", "createSubategory", "store_subcategory_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusCreated, map[string]string{"status": "ok", "id": sub.ID})
}

func listSubcategories(res *gateway.Response, req *gateway.Request) {
	categoryID, ok := req.URL.Query()["category_id"]

	if !ok || len(categoryID) <= 0 {
		logger.Error("server/handler/subcategory.go", "listSubcategories", "missing_category_id_parameter", logger.ErrObject(subcategory.ErrMissingID))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": subcategory.ErrMissingID.Error()})
		return
	}

	subcategories, err := subcategory.List(categoryID[0])
	if err != nil {
		logger.Error("server/handler/subcategory.go", "listSubcategories", "list_subcategories_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, subcategories)
}

func deleteSubcategory(res *gateway.Response, req *gateway.Request) {
	sub := &subcategory.Subcategory{}

	err := json.NewDecoder(req.Body).Decode(sub)
	if err != nil {
		logger.Error("server/handler/subcategory.go", "deleteSubcategory", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = sub.Delete()
	if err != nil {
		logger.Error("server/handler/subcategory.go", "deleteSubcategory", "delete_subcategory_failed")
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}

func updateSubcategory(res *gateway.Response, req *gateway.Request) {
	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/subcategory.go", "updateSubcategory", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	sub, err := subcategory.Load(data["id"])
	if err != nil {
		logger.Error("server/handler/subcategory.go", "updateSubcategory", "load_subcategory_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = sub.Update(data["name"], data["image"], data["category_id"])
	if err != nil {
		logger.Error("server/handler/subcategory.go", "updateSubcategory", "update_subcategory_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}
