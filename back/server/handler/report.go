package handler

import (
	"encoding/json"
	"net/http"
	"univalle/desarrollo/proyecto/back/reports"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
)

func report(res *gateway.Response, req *gateway.Request) {
	rep := &reports.Report{}

	err := json.NewDecoder(req.Body).Decode(rep)
	if err != nil {
		logger.Error("server/handler/report.go", "report", "decode_request_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = rep.GenerateReport()
	if err != nil {
		logger.Error("server/handler/report.go", "report", "generate_report_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusCreated, rep)
}
