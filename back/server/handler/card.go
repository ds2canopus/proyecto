package handler

import (
	"encoding/json"
	"net/http"
	"univalle/desarrollo/proyecto/back/models/card"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
)

func storeCard(res *gateway.Response, req *gateway.Request) {
	crd := &card.Card{}

	err := json.NewDecoder(req.Body).Decode(crd)
	if err != nil {
		logger.Error("server/handler/card.go", "createCard", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = crd.Store()
	if err != nil {
		logger.Error("server/handler/card.go", "createCard", "store_card_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusCreated, map[string]string{"status": "ok"})
}

func listCards(res *gateway.Response, req *gateway.Request) {
	userID, ok := req.URL.Query()["user_id"]

	if !ok || len(userID) <= 0 {
		logger.Error("server/handler/card.go", "listCards", "missing_user_id_parameter", logger.ErrObject(card.ErrMissingUserID))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": card.ErrMissingUserID.Error()})
		return
	}

	cards, err := card.List(userID[0])
	if err != nil {
		logger.Error("server/handler/card.go", "listCards", "list_cards_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, cards)
}

func deleteCard(res *gateway.Response, req *gateway.Request) {
	crd := &card.Card{}

	err := json.NewDecoder(req.Body).Decode(crd)
	if err != nil {
		logger.Error("server/handler/card.go", "deleteCard", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = crd.Delete()
	if err != nil {
		logger.Error("server/handler/card.go", "deleteCard", "delete_card_failed")
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}

func loadCard(res *gateway.Response, req *gateway.Request) {
	id, ok := req.URL.Query()["card_id"]

	if !ok || len(id) <= 0 {
		logger.Error("server/handler/card.go", "loadCard", "missing_id_parameter", logger.ErrObject(card.ErrMissingID))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": card.ErrMissingID.Error()})
		return
	}

	crd, err := card.Load(id[0])
	if err != nil {
		logger.Error("server/handler/card.go", "loadCard", "load_card_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, crd)
}
