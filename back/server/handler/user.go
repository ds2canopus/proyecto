package handler

import (
	"encoding/json"
	"errors"
	"net/http"

	"univalle/desarrollo/proyecto/back/models/user"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
)

func loginUser(res *gateway.Response, req *gateway.Request) {
	credentials := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&credentials)
	if err != nil {
		logger.Error("server/handler/user.go", "loginUser", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	token, err := user.Login(credentials["email"], credentials["password"])
	if err != nil {
		logger.Error("server/handler/user.go", "loginUser", "user_login_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, map[string]string{"token": token})
}

func createUser(res *gateway.Response, req *gateway.Request) {
	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/user.go", "createUser", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	usr, err := user.New(data["email"], data["document_id"], data["document_type"], data["name"], data["address"], data["birth_date"], data["password"])
	if err != nil {
		logger.Error("server/handler/user.go", "createUser", "new_user_error", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	err = usr.Store()
	if err != nil {
		logger.Error("server/handler/user.go", "createUser", "store_user_error", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, map[string]string{"status": "ok"})
}

func loadUser(res *gateway.Response, req *gateway.Request) {
	email, ok := req.URL.Query()["email"]

	if !ok || len(email) <= 0 {
		logger.Error("server/handler/user.go", "loadUser", "missing_email_parameter", logger.ErrObject(errors.New("missing email parameter")))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": "missing email parameter"})
		return
	}

	usr, err := user.Load(email[0])
	if err != nil {
		logger.Error("server/handler/user.go", "loadUser", "load_user_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, usr)
}

func updateUser(res *gateway.Response, req *gateway.Request) {
	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/user.go", "updateUser", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	usr, err := user.Load(data["email"])
	if err != nil {
		logger.Error("server/handler/user.go", "updateUser", "load_user_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = usr.Update(data["name"], data["address"], data["birth_date"], data["password"])
	if err != nil {
		logger.Error("server/handler/user.go", "updateUser", "update_user_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, map[string]string{"status": "ok"})
}

func deleteUser(res *gateway.Response, req *gateway.Request) {
	email := req.Email

	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/user.go", "deleteUser", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	email = data["email"]

	err = user.Delete(email)
	if err != nil {
		logger.Error("server/handler/user.go", "deleteUser", "delete_user_failed")
		res.WithJSON(401, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, map[string]string{"status": "ok"})
}

func listUsers(res *gateway.Response, req *gateway.Request) {
	users, err := user.List()
	if err != nil {
		logger.Error("server/handler/user.go", "listUsers", "list_users_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(200, users)
}
