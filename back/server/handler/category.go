package handler

import (
	"encoding/json"
	"net/http"
	"univalle/desarrollo/proyecto/back/models/category"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
)

func createCategory(res *gateway.Response, req *gateway.Request) {
	cat := &category.Category{}

	err := json.NewDecoder(req.Body).Decode(cat)
	if err != nil {
		logger.Error("server/handler/category.go", "createCategory", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = cat.Store()
	if err != nil {
		logger.Error("server/handler/category.go", "createCategory", "store_category_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusCreated, map[string]string{"status": "ok", "id": cat.ID})
}

func listCategories(res *gateway.Response, req *gateway.Request) {
	categories, err := category.List()
	if err != nil {
		logger.Error("server/handler/category.go", "listCategories", "list_categories_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, categories)
}

func deleteCategory(res *gateway.Response, req *gateway.Request) {
	cat := &category.Category{}

	err := json.NewDecoder(req.Body).Decode(cat)
	if err != nil {
		logger.Error("server/handler/category.go", "deleteCategory", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = cat.Delete()
	if err != nil {
		logger.Error("server/handler/category.go", "deleteCategory", "delete_category_failed")
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}

func updateCategory(res *gateway.Response, req *gateway.Request) {
	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/category.go", "updateCategory", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	cat, err := category.Load(data["category_id"])
	if err != nil {
		logger.Error("server/handler/category.go", "updateCategory", "load_category_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = cat.Update(data["name"], data["description"], data["image"])
	if err != nil {
		logger.Error("server/handler/category.go", "updateCategory", "update_category_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}
