package handler

import (
	"encoding/json"
	"net/http"
	"univalle/desarrollo/proyecto/back/models/product"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
)

func createProduct(res *gateway.Response, req *gateway.Request) {
	prd := &product.Product{}

	err := json.NewDecoder(req.Body).Decode(prd)
	if err != nil {
		logger.Error("server/handler/product.go", "createProduct", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = prd.Store()
	if err != nil {
		logger.Error("server/handler/product.go", "createProduct", "store_product_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusCreated, map[string]string{"status": "ok"})
}

func listProducts(res *gateway.Response, req *gateway.Request) {
	subcategoryID, ok := req.URL.Query()["subcategory_id"]

	if !ok || len(subcategoryID) <= 0 {
		logger.Error("server/handler/product.go", "listProducts", "missing_subcategory_id_parameter", logger.ErrObject(product.ErrMissingID))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": product.ErrMissingID.Error()})
		return
	}

	products, err := product.List(subcategoryID[0])
	if err != nil {
		logger.Error("server/handler/product.go", "listProducts", "list_products_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, products)
}

func deleteProduct(res *gateway.Response, req *gateway.Request) {
	data := map[string]string{}

	err := json.NewDecoder(req.Body).Decode(&data)
	if err != nil {
		logger.Error("server/handler/product.go", "deleteProduct", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = product.Delete(data["id"])
	if err != nil {
		logger.Error("server/handler/product.go", "deleteProduct", "delete_product_failed")
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}

func updateProduct(res *gateway.Response, req *gateway.Request) {
	newPRD := &product.Product{Quantity: -1}

	err := json.NewDecoder(req.Body).Decode(&newPRD)
	if err != nil {
		logger.Error("server/handler/product.go", "updateProduct", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	prd, err := product.Load(newPRD.ID)
	if err != nil {
		logger.Error("server/handler/product.go", "updateProduct", "load_product_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = prd.Update(newPRD.SubcategoryID, newPRD.Name, newPRD.Description, newPRD.Image, newPRD.Price, newPRD.IVA, newPRD.Quantity)
	if err != nil {
		logger.Error("server/handler/product.go", "updateProduct", "update_product_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, map[string]string{"status": "ok"})
}
