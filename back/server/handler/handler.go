package handler

import (
	"errors"
	"net/http"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"

	"github.com/gorilla/mux"
)

var (
	// ErrUnauthorizedRequest when a request does not have the required level of authorization
	ErrUnauthorizedRequest = errors.New("unauthorized request")
)

type handler func(res *gateway.Response, req *gateway.Request)

func handle(level string, h handler) func(res http.ResponseWriter, req *http.Request) {
	return func(res http.ResponseWriter, req *http.Request) {
		request := &gateway.Request{Request: req}
		err := request.Authorize(level)
		if err == nil {
			h(&gateway.Response{ResponseWriter: res}, request)
			return
		}

		logger.Error("server/handler/handler.go", "handle", "authorization_failed", logger.ErrObject(err))
		res.WriteHeader(401)
		_, err = res.Write([]byte(`{"error":"unauthorized request"}`))
		if err != nil {
			logger.Error("server/handler/handler.go", "handle", "write_response_failed", logger.ErrObject(err))
		}
	}
}

// NewRouter with handles
func NewRouter() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/login/admin", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/admin", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/admin", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/admin", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/admin/delete", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/admin/list", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/login/user", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/user", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/user", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/user", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/user/delete", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/admin/list", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/category", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/category", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/category/delete", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/category", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/subcategory", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/subcategory", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/subcategory/delete", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/subcategory", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/product", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/product", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/product/delete", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/product", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/cart", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/cart/delete", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/cart/clear", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/card", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/card/delete", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/card/list", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/discount/list", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/discount/delete", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/discount", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/discount/user", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/purchase", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/invoice", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/invoice/list", handle("", corsHandler)).Methods("OPTIONS")
	router.HandleFunc("/report", handle("", corsHandler)).Methods("OPTIONS")

	router.HandleFunc("/login/admin", handle("", loginAdmin)).Methods("POST")
	router.HandleFunc("/admin", handle("", createAdmin)).Methods("POST")
	// TODO: restore access level to admin
	router.HandleFunc("/admin", handle("", loadAdmin)).Methods("GET")
	router.HandleFunc("/admin", handle("admin", updateAdmin)).Methods("PUT")
	router.HandleFunc("/admin/delete", handle("admin", deleteAdmin)).Methods("POST")
	router.HandleFunc("/admin/list", handle("admin", listAdmins)).Methods("GET")

	router.HandleFunc("/login/user", handle("", loginUser)).Methods("POST")
	router.HandleFunc("/user", handle("", createUser)).Methods("POST")
	router.HandleFunc("/user", handle("", loadUser)).Methods("GET")
	router.HandleFunc("/user", handle("", updateUser)).Methods("PUT")
	router.HandleFunc("/user/delete", handle("", deleteUser)).Methods("POST")
	router.HandleFunc("/admin/list", handle("admin", listUsers)).Methods("GET")

	// TODO: implement an endpoint to load only one category
	router.HandleFunc("/category", handle("", createCategory)).Methods("POST")
	router.HandleFunc("/category", handle("", listCategories)).Methods("GET")
	// TODO: restore access level to admin
	router.HandleFunc("/category/delete", handle("", deleteCategory)).Methods("POST")
	router.HandleFunc("/category", handle("", updateCategory)).Methods("PUT")

	// TODO: restore access level to admin
	router.HandleFunc("/subcategory", handle("", createSubcategory)).Methods("POST")
	router.HandleFunc("/subcategory", handle("", listSubcategories)).Methods("GET")
	router.HandleFunc("/subcategory/delete", handle("", deleteSubcategory)).Methods("POST")
	router.HandleFunc("/subcategory", handle("", updateSubcategory)).Methods("PUT")

	// TODO: restore access level to admin
	router.HandleFunc("/product", handle("", createProduct)).Methods("POST")
	router.HandleFunc("/product", handle("", listProducts)).Methods("GET")
	router.HandleFunc("/product/delete", handle("", deleteProduct)).Methods("POST")
	router.HandleFunc("/product", handle("", updateProduct)).Methods("PUT")

	router.HandleFunc("/cart", handle("", storeCart)).Methods("POST")
	router.HandleFunc("/cart", handle("user", listCart)).Methods("GET")
	router.HandleFunc("/cart/delete", handle("user", deleteCart)).Methods("POST")
	router.HandleFunc("/cart/clear", handle("user", clearCart)).Methods("POST")

	router.HandleFunc("/card", handle("", storeCard)).Methods("POST")
	router.HandleFunc("/card", handle("", loadCard)).Methods("GET")
	router.HandleFunc("/card/delete", handle("", deleteCard)).Methods("POST")
	router.HandleFunc("/card/list", handle("", listCards)).Methods("GET")

	router.HandleFunc("/discount", handle("", createDiscount)).Methods("POST")
	router.HandleFunc("/discount", handle("", listDiscounts)).Methods("GET")
	router.HandleFunc("/discount/user", handle("", listDiscountsUser)).Methods("GET")
	router.HandleFunc("/discount/delete", handle("", deleteDiscount)).Methods("POST")

	router.HandleFunc("/purchase", handle("", createPurchase)).Methods("POST")

	router.HandleFunc("/invoice", handle("", loadInvoiceDetail)).Methods("GET")
	router.HandleFunc("/invoice/list", handle("", listInvoices)).Methods("GET")

	router.HandleFunc("/report", handle("", report)).Methods("POST")

	return router
}
