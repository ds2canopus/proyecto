package handler

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"univalle/desarrollo/proyecto/back/models/cart"
	"univalle/desarrollo/proyecto/back/models/discount"
	"univalle/desarrollo/proyecto/back/models/invoice"
	ip "univalle/desarrollo/proyecto/back/models/invoice-product"
	"univalle/desarrollo/proyecto/back/models/payment"
	"univalle/desarrollo/proyecto/back/models/product"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
	shared "univalle/desarrollo/proyecto/back/shared/payment"
	"univalle/desarrollo/proyecto/back/storage"
)

var (
	// ErrWrongPercentage the percentage does not cover the total price
	ErrWrongPercentage = errors.New("the percentages do not cover the total price")
)

// Purchase represents a purchase
type Purchase struct {
	storage.TimeTracker
	UserID         string            `json:"user_id"`
	PaymentMethods []payment.Payment `json:"payment_methods"`
	DiscountID     string            `json:"discount_id"`
}

func (p *Purchase) makePurchase() error {
	// initial check
	if !p.arePercentagesValid() {
		return ErrWrongPercentage
	}

	// generate invoice
	invoiceID, err := p.generateInvoice()
	if err != nil {
		return err
	}

	// generate invoice products
	invoiceTotal, totalIVA, err := p.generateInvoiceProducts(invoiceID)
	if err != nil {
		return err
	}

	// generate payments
	err = p.createPayments(invoiceID)
	if err != nil {
		return err
	}

	// get payment values
	discountMade, totalPayed := p.calculateDiscountAndTotalPayed(invoiceTotal, totalIVA)

	// update invoice
	err = p.updateInvoice(invoiceID, invoiceTotal, totalPayed, discountMade, totalIVA)
	if err != nil {
		return err
	}

	// update product quantities
	return p.updateInventory(invoiceID)
}

func (p *Purchase) updateInventory(invoiceID string) error {
	invoiceProducts, err := ip.List(invoiceID)
	if err != nil {
		return err
	}

	for _, invoiceProduct := range invoiceProducts {
		prod, err := product.Load(invoiceProduct.ProductID)
		if err != nil {
			return err
		}

		err = prod.Discount(prod.Quantity - invoiceProduct.Quantity)
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *Purchase) generateInvoice() (string, error) {
	i := &invoice.Invoice{
		UserID: p.UserID,
	}

	err := i.Store()
	if err != nil {
		return "", err
	}

	return i.ID, nil
}

func (p *Purchase) generateInvoiceProducts(invoiceID string) (float64, float64, error) {
	// get cart products
	cartContent, err := cart.List(p.UserID)
	if err != nil {
		return 0, 0, err
	}

	// calculate total and generate registers
	var total float64
	var totalIVA float64

	for _, cart := range cartContent {
		prod, err := product.Load(cart.ProductID)
		if err != nil {
			return 0, 0, err
		}

		if prod.Quantity < cart.Quantity {
			return 0, 0, errors.New("No hay suficientes unidades de " + prod.Name + " en la tienda. Unidades disponibles: " + strconv.Itoa(prod.Quantity))
		}

		i := &ip.InvoiceProduct{
			InvoiceID: invoiceID,
			ProductID: prod.ID,
			Quantity:  cart.Quantity,
			Price:     prod.Price,
			IVA:       prod.IVA,
		}

		err = i.Store()
		if err != nil {
			return 0, 0, err
		}

		total += i.Price * float64(i.Quantity)
		totalIVA += total * i.IVA / 100
	}

	_ = cart.Clear(p.UserID)

	return total, totalIVA, nil
}

func (p *Purchase) createPayments(invoiceID string) error {
	for _, paymentMethod := range p.PaymentMethods {
		paymentMethod.InvoiceID = invoiceID

		if paymentMethod.Type == shared.PaymentTypeCash {
			return nil
		}

		err := paymentMethod.Store()
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *Purchase) arePercentagesValid() bool {
	var totalPercentage float64

	for _, paymentMethod := range p.PaymentMethods {
		totalPercentage += paymentMethod.Percentage
	}

	if totalPercentage != 100 {
		return false
	}

	return true
}

func (p *Purchase) getDiscount() float64 {
	if p.DiscountID == "" {
		return 0
	}

	dis, err := discount.Load(p.DiscountID)
	if err != nil {
		return 0
	}

	_ = dis.UpdateStatus()

	return dis.Percentage
}

func (p *Purchase) calculateDiscountAndTotalPayed(total, iva float64) (float64, float64) {
	discountPercentage := p.getDiscount()

	subtotal := total + iva

	discountValue := (discountPercentage / 100) * subtotal
	totalPayed := subtotal - discountValue

	return discountValue, totalPayed
}

func (p *Purchase) updateInvoice(invoiceID string, invoiceTotal, totalPayed, discountMade, totalIVA float64) error {
	i := &invoice.Invoice{
		ID: invoiceID,
	}

	return i.Update(invoiceTotal, totalPayed, discountMade, totalIVA)
}

func createPurchase(res *gateway.Response, req *gateway.Request) {
	pur := &Purchase{}

	err := json.NewDecoder(req.Body).Decode(pur)
	if err != nil {
		logger.Error("server/handler/purchase.go", "createPurchase", "decode_response_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	err = pur.makePurchase()
	if err == nil {
		res.WithJSON(http.StatusCreated, map[string]string{"status": "ok"})
		return
	}

	logger.Error("server/handler/purchase.go", "createPurchase", "create_purchase_failed", logger.ErrObject(err))

	if strings.Contains(err.Error(), "suficientes unidades") {
		res.WithJSON(http.StatusNoContent, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
}
