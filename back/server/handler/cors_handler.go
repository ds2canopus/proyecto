package handler

import (
	"univalle/desarrollo/proyecto/back/shared/gateway"
)

func corsHandler(res *gateway.Response, req *gateway.Request) {
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "Authorizer")
	res.Header().Add("Access-Control-Allow-Headers", "Content-Type")
	res.Header().Set("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS")
}
