package handler

import (
	"net/http"
	"univalle/desarrollo/proyecto/back/models/invoice"
	ip "univalle/desarrollo/proyecto/back/models/invoice-product"
	"univalle/desarrollo/proyecto/back/shared/gateway"
	"univalle/desarrollo/proyecto/back/shared/logger"
	"univalle/desarrollo/proyecto/back/storage"
)

// InvoiceContent represents an invoice detail
type InvoiceContent struct {
	storage.TimeTracker
	Info     *invoice.Invoice     `json:"info"`
	Products []*ip.InvoiceProduct `json:"products"`
}

func (i *InvoiceContent) getInvoice(invoiceID string) error {
	// get general invoice
	genInvoice, err := invoice.Load(invoiceID)
	if err != nil {
		return err
	}

	// get products
	invoiceProducts, err := ip.List(invoiceID)
	if err != nil {
		return err
	}

	// build invoice content
	i.Info = genInvoice
	i.Products = invoiceProducts

	return nil
}

func loadInvoiceDetail(res *gateway.Response, req *gateway.Request) {
	invoiceID, ok := req.URL.Query()["invoice_id"]

	if !ok || len(invoiceID) == 0 {
		logger.Error("server/handler/invoice.go", "createInvoice", "missing_invoice_id", logger.ErrObject(invoice.ErrMissingID))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": invoice.ErrMissingID.Error()})
		return
	}

	inv := &InvoiceContent{}

	err := inv.getInvoice(invoiceID[0])
	if err != nil {
		logger.Error("server/handler/invoice.go", "loadInvoice", "create_invoice_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusCreated, inv)
}

func listInvoices(res *gateway.Response, req *gateway.Request) {
	userID, ok := req.URL.Query()["user_id"]

	if !ok || len(userID) == 0 {
		logger.Error("server/handler/invoice.go", "listInvoices", "missing_invoice_id", logger.ErrObject(invoice.ErrMissingUserID))
		res.WithJSON(http.StatusBadRequest, map[string]string{"error": invoice.ErrMissingUserID.Error()})
		return
	}

	invoices, err := invoice.List(userID[0])
	if err != nil {
		logger.Error("server/handler/invoice.go", "listInvoices", "list_invoices_failed", logger.ErrObject(err))
		res.WithJSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	}

	res.WithJSON(http.StatusOK, invoices)
}
