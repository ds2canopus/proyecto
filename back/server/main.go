package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"univalle/desarrollo/proyecto/back/models/admin"
	"univalle/desarrollo/proyecto/back/models/card"
	"univalle/desarrollo/proyecto/back/models/cart"
	"univalle/desarrollo/proyecto/back/models/category"
	"univalle/desarrollo/proyecto/back/models/discount"
	"univalle/desarrollo/proyecto/back/models/invoice"
	invoiceProduct "univalle/desarrollo/proyecto/back/models/invoice-product"
	"univalle/desarrollo/proyecto/back/models/payment"
	"univalle/desarrollo/proyecto/back/models/product"
	"univalle/desarrollo/proyecto/back/models/subcategory"
	"univalle/desarrollo/proyecto/back/models/user"
	"univalle/desarrollo/proyecto/back/server/handler"
	"univalle/desarrollo/proyecto/back/shared/logger"
	"univalle/desarrollo/proyecto/back/storage"

	"github.com/joho/godotenv"
)

func init() {
	err := godotenv.Load("../vars.env")
	if err != nil {
		logger.Fatal("server/main.go", "main", "load_env_vars_failed", logger.ErrObject(err))
		panic(err)
	}
}

func initDB() func() {
	closer := storage.Connect()

	storage.AddTable(&admin.Admin{})
	storage.AddTable(&user.User{})
	storage.AddTable(&category.Category{})
	storage.AddTable(&subcategory.Subcategory{}, storage.ForeignKey("category_id", "category(category_id)"))
	storage.AddTable(&product.Product{}, storage.ForeignKey("subcategory_id", "subcategory(subcategory_id)"))
	storage.AddTable(&cart.Cart{}, storage.ForeignKey("user_id", "users(email)"), storage.ForeignKey("product_id", "product(product_id)"))
	storage.AddTable(&invoice.Invoice{}, storage.ForeignKey("user_id", "users(email)"))
	storage.AddTable(&invoiceProduct.InvoiceProduct{}, storage.ForeignKey("invoice_id", "invoice(invoice_id)"), storage.ForeignKey("product_id", "product(product_id)"))
	storage.AddTable(&discount.Discount{}, storage.ForeignKey("user_id", "users(email)"))
	storage.AddTable(&card.Card{}, storage.ForeignKey("user_id", "users(email)"))
	storage.AddTable(&payment.Payment{}, storage.ForeignKey("invoice_id", "invoice(invoice_id)"), storage.ForeignKey("card_id", "card(card_id)"))

	return closer
}

func main() {
	closeDB := initDB()
	defer closeDB()

	server := &http.Server{
		Addr:           os.Getenv("server_port"),
		Handler:        handler.NewRouter(),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	fmt.Println("Starting server")
	log.Fatal(server.ListenAndServe())
}
