package reports

import (
	"bytes"
	"encoding/base64"
	"errors"
	"strconv"
	"time"
	"univalle/desarrollo/proyecto/back/storage"

	"github.com/wcharczuk/go-chart"
)

var (
	// ErrMissingToDate missing To date
	ErrMissingToDate = errors.New("missing_to_date")
	// ErrMissingFromDate missing From date
	ErrMissingFromDate = errors.New("missing_from_date")
	// ErrTooManyDays too many days in range
	ErrTooManyDays = errors.New("too_many_days_in_range")
	// ErrMissingID missing id
	ErrMissingID = errors.New("missing_id")
)

var (
	reportsMap = map[ReportType]func(*Report) error{
		Top20MoreSold:       top20MoreSold,
		Top20LessSOld:       top20LessSold,
		LoyalUsers:          loyalUsers,
		SalesFromDate:       salesFromDate,
		TotalSalesProduct:   totalSalesProduct,
		LowQuantityProducts: lowQuantityProducts,
		NextUsersBirthdays:  nextUsersBirthdays,
	}

	months = map[int]string{
		1:  "Enero",
		2:  "Febrero",
		3:  "Marzo",
		4:  "Abril",
		5:  "Mayo",
		6:  "Junio",
		7:  "Julio",
		8:  "Agosto",
		9:  "Septiembre",
		10: "Octubre",
		11: "Noviembre",
		12: "Diciembre",
	}
)

func generateTicks(maxValue int) []chart.Tick {
	delta := 0

	switch {
	case maxValue < 10:
		delta = 1
	case maxValue < 20:
		delta = 2
	case maxValue < 50:
		delta = 5
	case maxValue < 100:
		delta = 10
	case maxValue < 1000:
		delta = 100
	default:
		delta = 1000
	}

	ticks := []chart.Tick{}
	for i := 0; i <= maxValue; i += delta {
		ticks = append(ticks, chart.Tick{Label: strconv.Itoa(i), Value: float64(i)})
	}

	return ticks
}

func top20MoreSold(r *Report) error {
	rows, err := storage.DB.
		Order("quantity desc").
		Limit(20).
		Table("invoice_product").
		Select("product.name, sum(invoice_product.quantity) as quantity").
		Joins("left join product on product.product_id = invoice_product.product_id").
		Group("invoice_product.product_id, product.name").
		Rows()
	if err != nil {
		return err
	}

	bars := []chart.Value{}
	label := ""
	value := 0
	maxValue := 0
	for rows.Next() {
		err := rows.Scan(&label, &value)
		if err != nil {
			return err
		}

		if value > maxValue {
			maxValue = value
		}

		bars = append(bars, chart.Value{
			Label: label,
			Value: float64(value),
		})
	}

	if len(bars) == 0 {
		*r = Report{
			Info: "No records found",
		}

		return nil
	}

	graph := chart.BarChart{
		Title:      "Top 20 productos más vendidos",
		TitleStyle: chart.Shown(),
		Background: chart.Style{
			Padding: chart.Box{
				Top: 40,
			},
		},
		Height:   512,
		Width:    232 + (36 * len(bars)),
		BarWidth: 30,
		XAxis: chart.Style{
			Hidden:              false,
			TextRotationDegrees: 90,
		},
		YAxis: chart.YAxis{
			Name: "Cantidad",
			NameStyle: chart.Style{
				Hidden:              false,
				TextRotationDegrees: 90,
			},
			Style: chart.Shown(),
			Range: &chart.ContinuousRange{
				Min: 0.0,
				Max: float64(maxValue),
			},
			Ticks: generateTicks(maxValue),
		},
		Bars: bars,
	}

	buffer := bytes.NewBuffer([]byte{})
	err = graph.Render(chart.PNG, buffer)
	if err != nil {
		return err
	}

	*r = Report{
		Image: string(base64.StdEncoding.EncodeToString(buffer.Bytes())),
	}
	return nil
}

func top20LessSold(r *Report) error {
	rows, err := storage.DB.
		Order("quantity asc").
		Limit(20).
		Table("invoice_product").
		Select("product.name, sum(invoice_product.quantity) as quantity").
		Joins("left join product on product.product_id = invoice_product.product_id").
		Group("invoice_product.product_id, product.name").
		Rows()
	if err != nil {
		return err
	}

	bars := []chart.Value{}
	label := ""
	value := 0
	maxValue := 0
	for rows.Next() {
		err := rows.Scan(&label, &value)
		if err != nil {
			return err
		}

		if value > maxValue {
			maxValue = value
		}

		bars = append(bars, chart.Value{
			Label: label,
			Value: float64(value),
		})
	}

	if len(bars) == 0 {
		*r = Report{
			Info: "No records found",
		}

		return nil
	}

	graph := chart.BarChart{
		Title:      "Top 20 productos más vendidos",
		TitleStyle: chart.Shown(),
		Background: chart.Style{
			Padding: chart.Box{
				Top: 40,
			},
		},
		Height:   512,
		Width:    232 + (36 * len(bars)),
		BarWidth: 30,
		XAxis: chart.Style{
			Hidden:              false,
			TextRotationDegrees: 90,
		},
		YAxis: chart.YAxis{
			Name: "Cantidad",
			NameStyle: chart.Style{
				Hidden:              false,
				TextRotationDegrees: 90,
			},
			Style: chart.Shown(),
			Range: &chart.ContinuousRange{
				Min: 0.0,
				Max: float64(maxValue),
			},
			Ticks: generateTicks(maxValue),
		},
		Bars: bars,
	}

	buffer := bytes.NewBuffer([]byte{})
	err = graph.Render(chart.PNG, buffer)
	if err != nil {
		return err
	}

	*r = Report{
		Image: string(base64.StdEncoding.EncodeToString(buffer.Bytes())),
	}
	return nil
}

func loyalUsers(r *Report) error {
	rows, err := storage.DB.
		Order("price asc").
		Limit(10).
		Table("invoice_product").
		Select("invoice.user_id, sum(invoice_product.price) as price").
		Joins("left join invoice on invoice.invoice_id = invoice_product.invoice_id").
		Group("invoice.user_id").
		Rows()
	if err != nil {
		return err
	}

	bars := []chart.Value{}
	label := ""
	value := 0
	maxValue := 0
	for rows.Next() {
		err := rows.Scan(&label, &value)
		if err != nil {
			return err
		}

		if value > maxValue {
			maxValue = value
		}

		bars = append(bars, chart.Value{
			Label: label,
			Value: float64(value),
		})
	}

	if len(bars) == 0 {
		*r = Report{
			Info: "No records found",
		}

		return nil
	}

	graph := chart.BarChart{
		Title:      "Most loyal users",
		TitleStyle: chart.Shown(),
		Background: chart.Style{
			Padding: chart.Box{
				Top: 40,
			},
		},
		Height:   512,
		Width:    232 + (36 * len(bars)),
		BarWidth: 30,
		XAxis: chart.Style{
			Hidden:              true,
			TextRotationDegrees: 90,
		},
		YAxis: chart.YAxis{
			Name: "Cantidad",
			NameStyle: chart.Style{
				Hidden:              false,
				TextRotationDegrees: 90,
			},
			Style: chart.Shown(),
			Range: &chart.ContinuousRange{
				Min: 0.0,
				Max: float64(maxValue),
			},
			Ticks: generateTicks(maxValue),
		},
		Bars: bars,
	}

	buffer := bytes.NewBuffer([]byte{})
	err = graph.Render(chart.PNG, buffer)
	if err != nil {
		return err
	}

	*r = Report{
		Image: string(base64.StdEncoding.EncodeToString(buffer.Bytes())),
	}
	return nil
}

func salesFromDate(r *Report) error {
	if r.To == nil {
		return ErrMissingToDate
	}

	if r.From == nil {
		return ErrMissingFromDate
	}

	diff := r.To.Sub(*r.From)
	if diff > (time.Hour * 24 * 7) {
		return ErrTooManyDays
	}

	rows, err := storage.DB.
		Order("sale_day asc").
		Table("invoice").
		Select("COUNT(invoice.invoice_id) AS quantity, EXTRACT(DAY FROM invoice.created_at AT TIME ZONE '-5') AS sale_day").
		Where("invoice.created_at BETWEEN ? AND ?", r.From, r.To).
		Group("sale_day").
		Rows()
	if err != nil {
		return err
	}

	bars := []chart.Value{}
	label := 0
	value := 0
	maxValue := 0
	for rows.Next() {
		err := rows.Scan(&value, &label)
		if err != nil {
			return err
		}

		if value > maxValue {
			maxValue = value
		}

		bars = append(bars, chart.Value{
			Label: strconv.Itoa(label),
			Value: float64(value),
		})
	}

	if len(bars) == 0 {
		*r = Report{
			Info: "No records found",
		}

		return nil
	}

	graph := chart.BarChart{
		Title:      "Sales",
		TitleStyle: chart.Shown(),
		Background: chart.Style{
			Padding: chart.Box{
				Top: 40,
			},
		},
		Height:   512,
		Width:    232 + (36 * len(bars)),
		BarWidth: 30,
		XAxis: chart.Style{
			Hidden:              false,
			TextRotationDegrees: 90,
		},
		YAxis: chart.YAxis{
			Name: "Cantidad",
			NameStyle: chart.Style{
				Hidden:              false,
				TextRotationDegrees: 90,
			},
			Style: chart.Shown(),
			Range: &chart.ContinuousRange{
				Min: 0.0,
				Max: float64(maxValue),
			},
			Ticks: generateTicks(maxValue),
		},
		Bars: bars,
	}

	buffer := bytes.NewBuffer([]byte{})
	err = graph.Render(chart.PNG, buffer)
	if err != nil {
		return err
	}

	*r = Report{
		Image: string(base64.StdEncoding.EncodeToString(buffer.Bytes())),
	}

	return nil
}

func totalSalesProduct(r *Report) error {
	if r.ID == "" {
		return ErrMissingID
	}
	month := int(time.Now().AddDate(0, -6, 0).Month())

	rows, err := storage.DB.
		Order("month_sold asc").
		Table("invoice_product").
		Select("SUM(invoice_product.quantity) AS quantity, EXTRACT(MONTH FROM invoice_product.created_at AT TIME ZONE '-5') AS month_sold").
		Where("invoice_product.product_id = ? AND EXTRACT(MONTH FROM invoice_product.created_at AT TIME ZONE '-5') > ?", r.ID, strconv.Itoa(month)).
		Group("invoice_product.quantity, month_sold").
		Rows()

	if err != nil {
		return err
	}

	bars := []chart.Value{}
	label := 0
	value := 0
	maxValue := 0
	for rows.Next() {
		err := rows.Scan(&value, &label)
		if err != nil {
			return err
		}

		if value > maxValue {
			maxValue = value
		}

		bars = append(bars, chart.Value{
			Label: months[label],
			Value: float64(value),
		})
	}

	if len(bars) == 0 {
		*r = Report{
			Info: "No records found",
		}

		return nil
	}

	graph := chart.BarChart{
		Title:      "Last 6 month sales",
		TitleStyle: chart.Shown(),
		Background: chart.Style{
			Padding: chart.Box{
				Top: 40,
			},
		},
		Height:   512,
		Width:    232 + (36 * len(bars)),
		BarWidth: 30,
		XAxis: chart.Style{
			Hidden:              false,
			TextRotationDegrees: 90,
		},
		YAxis: chart.YAxis{
			Name: "Cantidad",
			NameStyle: chart.Style{
				Hidden:              false,
				TextRotationDegrees: 90,
			},
			Style: chart.Shown(),
			Range: &chart.ContinuousRange{
				Min: 0.0,
				Max: float64(maxValue),
			},
			Ticks: generateTicks(maxValue),
		},
		Bars: bars,
	}

	buffer := bytes.NewBuffer([]byte{})
	err = graph.Render(chart.PNG, buffer)
	if err != nil {
		return err
	}

	*r = Report{
		Image: string(base64.StdEncoding.EncodeToString(buffer.Bytes())),
	}

	return nil
}

func lowQuantityProducts(r *Report) error {
	rows, err := storage.DB.
		Table("product").
		Select("product.product_id, product.name, product.quantity").
		Where("product.quantity < ?", "10").
		Rows()
	if err != nil {
		return err
	}

	id := ""
	label := ""
	value := 0
	result := []*Result{}
	for rows.Next() {
		err := rows.Scan(&id, &label, &value)
		if err != nil {
			return err
		}

		result = append(result, &Result{
			ID:    id,
			Label: label,
			Value: strconv.Itoa(value),
		})
	}

	if len(result) == 0 {
		*r = Report{
			Info: "No records found",
		}

		return nil
	}

	*r = Report{
		Result: result,
	}
	return nil
}

func nextUsersBirthdays(r *Report) error {
	month := int(time.Now().AddDate(0, 1, 0).Month())
	rows, err := storage.DB.
		Table("users").
		Select("users.email, users.birth_date").
		Where("EXTRACT(MONTH FROM users.birth_date AT TIME ZONE '-5') = ?", strconv.Itoa(month)).
		Rows()
	if err != nil {
		return err
	}

	label := ""
	value := ""
	result := []*Result{}
	for rows.Next() {
		err := rows.Scan(&label, &value)
		if err != nil {
			return err
		}

		result = append(result, &Result{
			Label: label,
			Value: value,
		})
	}

	if len(result) == 0 {
		*r = Report{
			Info: "No records found",
		}

		return nil
	}

	*r = Report{
		Result: result,
	}
	return nil
}
