package reports

import (
	"errors"
	"time"
)

// ErrWrongReportType when a report type is unknown
var ErrUnknownReportType = errors.New("unknown_report_type")

type Result struct {
	ID    string `json:"id,omitempty"`
	Label string `json:"label,omitempty"`
	Value string `json:"value,omitempty"`
}

type Report struct {
	Type   ReportType `json:"report_type,omitempty"`
	ID     string     `json:"id,omitempty"`
	From   *time.Time `json:"from,omitempty"`
	To     *time.Time `json:"to,omitempty"`
	Image  string     `json:"image,omitempty"`
	Result []*Result  `json:"result,omitempty"`
	Info   string     `json:"info,omitempty"`
}

func (r *Report) GenerateReport() error {
	report, ok := reportsMap[r.Type]
	if !ok {
		return ErrUnknownReportType
	}

	err := report(r)
	if err != nil {
		return err
	}

	return nil
}
