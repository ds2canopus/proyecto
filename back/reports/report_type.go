package reports

type ReportType string

const (
	Top20MoreSold       ReportType = "top_20_more_sold"
	Top20LessSOld       ReportType = "top_20_less_sold"
	LoyalUsers          ReportType = "loyal_users"
	SalesFromDate       ReportType = "sales_from_date"
	TotalSalesProduct   ReportType = "total_sales_product"
	LowQuantityProducts ReportType = "low_quantity_products"
	NextUsersBirthdays  ReportType = "next_users_birthdays"
)
