package storage

// Option is a secundary operation of the database
type Option func(v interface{})

// ForeignKey adds a foreign key relation
func ForeignKey(field, dest string) Option {
	return func(v interface{}) {
		db := DB.Model(v).AddForeignKey(field, dest, "CASCADE", "CASCADE")
		if db.Error != nil {
			panic(db.Error)
		}
	}
}
