package storage

import "time"

// TimeTracker holds timestamp information
type TimeTracker struct {
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-"`
}
