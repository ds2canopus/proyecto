package storage

import (
	"fmt"
	"os"
	"sync"

	"univalle/desarrollo/proyecto/back/shared/logger"

	"github.com/jinzhu/gorm"
	// Imported to handle postgres connections
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	// DB is a connection to the dabase
	DB *gorm.DB

	once sync.Once
)

func close() {
	if err := DB.Close(); err != nil {
		logger.Error("storage/client.go", "close", "close_database_connection_failed", logger.ErrObject(err))
	}
}

// Connect to the database and returns a function to close the connection
func Connect() func() {
	username := os.Getenv("postgres_user")
	password := os.Getenv("postgres_password")
	port := os.Getenv("postgres_port")
	dbName := os.Getenv("database_name")
	dbHost := os.Getenv("database_host")

	dbURI := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, port, username, dbName, password)

	once.Do(func() {
		conn, err := gorm.Open("postgres", dbURI)
		if err != nil {
			logger.Fatal("storage/client.go", "init", "init_database_connection_failed", logger.ErrObject(err))
			panic(err)
		}

		DB = conn
		logger.Info("storage/client.go", "init", "database_connection_created_succesfully")
	})

	return close
}

// AddTable to the database
func AddTable(v interface{}, options ...Option) {
	if !DB.HasTable(v) {
		db := DB.CreateTable(v)
		if db.Error != nil {
			panic(db.Error)
		}
	}

	for _, opt := range options {
		opt(v)
	}
}
