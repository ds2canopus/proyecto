package category

import (
	"errors"
	"univalle/desarrollo/proyecto/back/shared/random"
	"univalle/desarrollo/proyecto/back/storage"
)

const categoryIDPrefix = "CAT"

var (
	// ErrMissingID the category id is missing
	ErrMissingID = errors.New("missing category id")
	// ErrMissingName the category name is missing
	ErrMissingName = errors.New("missing category name")
	// ErrMissingDescription the category description is missing
	ErrMissingDescription = errors.New("missing category description")
	// ErrMissingImage the category image is missing
	ErrMissingImage = errors.New("missing category image")
)

// Category handles a category information
type Category struct {
	storage.TimeTracker
	ID          string `json:"category_id" gorm:"primary_key;column:category_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Image       string `json:"image"`
}

// TableName in the database
func (c *Category) TableName() string {
	return "category"
}

// Store a category in the database
func (c *Category) Store() error {
	if c.Name == "" {
		return ErrMissingName
	}

	if c.Description == "" {
		return ErrMissingDescription
	}

	if c.Image == "" {
		return ErrMissingImage
	}

	c.ID = random.GenerateID(categoryIDPrefix)

	return storage.DB.Create(c).Error
}

// List the stored categories
func List() ([]*Category, error) {
	list := []*Category{}

	err := storage.DB.Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Delete a category
func (c *Category) Delete() error {
	if c.ID == "" {
		return ErrMissingID
	}

	return storage.DB.Delete(c).Error
}

// Update a category
func (c *Category) Update(name, description, image string) error {
	if c.ID == "" {
		return ErrMissingID
	}

	if name != "" {
		c.Name = name
	}

	if description != "" {
		c.Description = description
	}

	if image != "" {
		c.Image = image
	}

	return storage.DB.Model(c).Updates(c).Error
}

// Load a category info
func Load(id string) (*Category, error) {
	if id == "" {
		return nil, ErrMissingID
	}

	category := &Category{}

	err := storage.DB.First(category, "category_id = ?", id).Error
	if err != nil {
		return nil, err
	}

	return category, nil
}
