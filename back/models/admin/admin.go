package admin

import (
	"errors"

	"univalle/desarrollo/proyecto/back/shared/authentication"
	"univalle/desarrollo/proyecto/back/storage"
)

var (
	// ErrMissingEmail when email is missing
	ErrMissingEmail = errors.New("missing email")

	// ErrMissingName when name is missing
	ErrMissingName = errors.New("missing name")

	// ErrMissingPassword when password is missing
	ErrMissingPassword = errors.New("missing password")
)

// Admin is an administrator of the store
type Admin struct {
	storage.TimeTracker
	Email    string `json:"email" gorm:"primary_key;column:email"`
	Name     string `json:"name" gorm:"column:name"`
	Password string `json:"password" gorm:"column:password"`
}

// TableName in the database
func (a *Admin) TableName() string {
	return "admin"
}

// Store an admin in the database
func (a *Admin) Store() error {
	return storage.DB.Create(a).Error
}

// Update an admin
func (a *Admin) Update(name, password string) error {
	fields := map[string]interface{}{}

	if name != "" {
		fields["name"] = name
	}

	if password != "" {
		hash, err := authentication.HashPassword(password)
		if err != nil {
			return nil
		}
		fields["password"] = hash
	}

	return storage.DB.Model(a).Updates(fields).Error
}

// New creates an admin and hash the given password
func New(email, name, password string) (*Admin, error) {
	if email == "" {
		return nil, ErrMissingEmail
	}

	if name == "" {
		return nil, ErrMissingName
	}

	if password == "" {
		return nil, ErrMissingPassword
	}

	hash, err := authentication.HashPassword(password)
	if err != nil {
		return nil, err
	}

	return &Admin{
		Email:    email,
		Name:     name,
		Password: string(hash),
	}, nil
}

// Login verifies the given credentials and returns a JWT
func Login(email, password string) (string, error) {
	if email == "" {
		return "", ErrMissingEmail
	}

	if password == "" {
		return "", ErrMissingPassword
	}

	admin := &Admin{}

	db := storage.DB.First(admin, "email = ?", email)
	if db.Error != nil {
		return "", db.Error
	}

	err := authentication.CompareHash(admin.Password, password)
	if err != nil {
		return "", err
	}

	token, err := authentication.GenerateToken(map[string]interface{}{
		"email": admin.Email,
		"level": "admin",
	})

	if err != nil {
		return "", err
	}

	return token, nil
}

// Load searchs and returns an admin
func Load(email string) (*Admin, error) {
	if email == "" {
		return nil, ErrMissingEmail
	}

	admin := &Admin{}

	db := storage.DB.First(admin, "email = ?", email)
	if db.Error != nil {
		return nil, db.Error
	}

	return admin, nil
}

// Delete an admin
func Delete(email string) error {
	if email == "" {
		return ErrMissingEmail
	}

	return storage.DB.Where("email = ?", email).Delete(&Admin{}).Error
}

// List the stored admins
func List() ([]*Admin, error) {
	list := []*Admin{}
	if err := storage.DB.Find(&list).Error; err != nil {
		return nil, err
	}

	return list, nil
}
