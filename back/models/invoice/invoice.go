package invoice

import (
	"errors"
	"time"
	"univalle/desarrollo/proyecto/back/shared/random"
	"univalle/desarrollo/proyecto/back/storage"
)

const invoiceIDPrefix = "INV"

var (
	// ErrMissingID the invoice id is missing
	ErrMissingID = errors.New("missing invoice id")
	// ErrMissingUserID the user id is missing
	ErrMissingUserID = errors.New("missing user id")
)

// Invoice represents a user invoice
type Invoice struct {
	storage.TimeTracker
	ID              string    `json:"invoice_id" gorm:"primary_key;column:invoice_id"`
	UserID          string    `json:"user_id"`
	Date            string    `json:"date"`
	Discount        float64   `json:"discount"`
	Total           float64   `json:"total"`
	TotalPayed      float64   `json:"total_payed"`
	TotalIVA        float64   `json:"total_iva"`
	StatusCompleted bool      `json:"status_completed"`
	ApprovalNumber  string    `json:"approval_number"`
	ApprovalEntity  string    `json:"approval_entity"`
	ApprovalDate    time.Time `json:"approval_date"`
}

// TableName in the database
func (i *Invoice) TableName() string {
	return "invoice"
}

// Store an invoice in the database
func (i *Invoice) Store() error {
	if i.UserID == "" {
		return ErrMissingUserID
	}

	i.ID = random.GenerateID(invoiceIDPrefix)
	i.ApprovalNumber = random.Number()
	i.ApprovalEntity = random.RandomEntity()
	i.ApprovalDate = time.Now()
	i.Date = time.Now().String()
	i.StatusCompleted = false

	return storage.DB.Create(i).Error
}

// Update updates the invoice total and discount once they are known
func (i *Invoice) Update(total, totalPayed, discount, totalIVA float64) error {
	fields := map[string]interface{}{
		"total":            total,
		"total_payed":      totalPayed,
		"discount":         discount,
		"status_completed": true,
		"total_iva":        totalIVA,
	}

	return storage.DB.Model(i).Updates(fields).Error
}

// List the stored invoices from a user
func List(userID string) ([]*Invoice, error) {
	list := []*Invoice{}

	err := storage.DB.Where("user_id = ? AND status_completed = true", userID).Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Load an invoice info
func Load(id string) (*Invoice, error) {
	if id == "" {
		return nil, ErrMissingID
	}

	inv := &Invoice{}

	err := storage.DB.First(inv, "invoice_id = ?", id).Error
	if err != nil {
		return nil, err
	}

	return inv, nil
}
