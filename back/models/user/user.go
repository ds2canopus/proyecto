package user

import (
	"errors"
	"time"

	"univalle/desarrollo/proyecto/back/shared/authentication"
	"univalle/desarrollo/proyecto/back/storage"
)

var (
	// ErrMissingEmail when email is missing
	ErrMissingEmail = errors.New("missing email")
	// ErrMissingName when name is missing
	ErrMissingName = errors.New("missing name")
	// ErrMissingBirthDate when birth date is missing
	ErrMissingBirthDate = errors.New("missing birth date")
	// ErrMissingAddress when address is missing
	ErrMissingAddress = errors.New("missing address")
	// ErrMissingPassword when password is missing
	ErrMissingPassword = errors.New("missing password")
	// ErrWrongBirthDate invalid birthdate
	ErrWrongBirthDate = errors.New("invalid birth date")
	// ErrMissingDocumentID when document id is missing
	ErrMissingDocumentID = errors.New("missing document id")
	// ErrMissingDocumentType when document type is missing
	ErrMissingDocumentType = errors.New("missing document type")
)

// User is a client user of the store
type User struct {
	storage.TimeTracker
	Email        string    `json:"email" gorm:"primary_key;column:email"`
	Name         string    `json:"name" gorm:"column:name"`
	BirthDate    time.Time `json:"birth_date" gorm:"column:birth_date"`
	Address      string    `json:"address" gorm:"column:address"`
	Password     string    `json:"-" gorm:"column:password"`
	Phone        string    `json:"phone" gorm:"column:phone"`
	DocumentID   string    `json:"document_id" gorm:"column:document_id"`
	DOcumentType string    `json:"document_type" gorm:"column:document_type"`
}

// TableName in the database
func (a *User) TableName() string {
	return "users"
}

// Store a client user in the database
func (a *User) Store() error {
	return storage.DB.Create(a).Error
}

// Update a client user
func (a *User) Update(name, address, birthDate, password string) error {
	fields := map[string]interface{}{}

	if name != "" {
		fields["name"] = name
	}

	if address != "" {
		fields["address"] = address
	}

	if birthDate != "" {
		birth, err := time.Parse(time.RFC3339, birthDate)
		if err != nil {
			return err
		}
		fields["birth_date"] = birth
	}

	if password != "" {
		hash, err := authentication.HashPassword(password)
		if err != nil {
			return nil
		}
		fields["password"] = hash
	}

	return storage.DB.Model(a).Updates(fields).Error
}

// New creates an admin and hash the given password
func New(email, documentID, documentType, name, address, birthDate, password string) (*User, error) {
	user := &User{}
	if email == "" {
		return nil, ErrMissingEmail
	}

	if documentID == "" {
		return nil, ErrMissingDocumentID
	}

	if documentType == "" {
		return nil, ErrMissingDocumentType
	}

	if name == "" {
		return nil, ErrMissingName
	}

	if address == "" {
		return nil, ErrMissingAddress
	}

	if birthDate != "" {
		birth, err := time.Parse(time.RFC3339, birthDate)
		if err != nil {
			return nil, err
		}

		if birth.Year() >= time.Now().Year() {
			return nil, ErrWrongBirthDate
		}

		user.BirthDate = birth
	}

	if password == "" {
		return nil, ErrMissingPassword
	}

	hash, err := authentication.HashPassword(password)
	if err != nil {
		return nil, err
	}

	user.Email = email
	user.Name = name
	user.Address = address
	user.Password = string(hash)
	return user, nil
}

// Login verifies the given credentials and returns a JWT
func Login(email, password string) (string, error) {
	if email == "" {
		return "", ErrMissingEmail
	}

	if password == "" {
		return "", ErrMissingPassword
	}

	user := &User{}

	db := storage.DB.First(user, "email = ?", email)
	if db.Error != nil {
		return "", db.Error
	}

	err := authentication.CompareHash(user.Password, password)
	if err != nil {
		return "", err
	}

	token, err := authentication.GenerateToken(map[string]interface{}{
		"email": user.Email,
		"level": "user",
	})

	if err != nil {
		return "", err
	}

	return token, nil
}

// Load searchs and returns a client user
func Load(email string) (*User, error) {
	if email == "" {
		return nil, ErrMissingEmail
	}

	user := &User{}

	db := storage.DB.First(user, "email = ?", email)
	if db.Error != nil {
		return nil, db.Error
	}

	return user, nil
}

// Delete a client user
func Delete(email string) error {
	if email == "" {
		return ErrMissingEmail
	}

	return storage.DB.Where("email = ?", email).Delete(&User{}).Error
}

// List the stored client users
func List() ([]*User, error) {
	list := []*User{}
	if err := storage.DB.Find(&list).Error; err != nil {
		return nil, err
	}

	return list, nil
}
