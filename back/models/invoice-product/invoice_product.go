package invoice

import (
	"errors"
	"univalle/desarrollo/proyecto/back/storage"
)

var (
	// ErrMissingInvoiceID the invoice id is missing
	ErrMissingInvoiceID = errors.New("missing invoice id")
	// ErrMissingProductID the product id is missing
	ErrMissingProductID = errors.New("missing product id")
	// ErrMissingQuantity the quantity is missing
	ErrMissingQuantity = errors.New("missing quantity")
	// ErrMissingPrice the price is missing
	ErrMissingPrice = errors.New("missing price")
)

// InvoiceProduct represents an invoice product
type InvoiceProduct struct {
	storage.TimeTracker
	InvoiceID string  `json:"invoice_id" gorm:"primary_key"`
	ProductID string  `json:"product_id" gorm:"primary_key"`
	Quantity  int     `json:"quantity"`
	Price     float64 `json:"price"`
	IVA       float64 `json:"iva"`
}

// TableName in the database
func (i *InvoiceProduct) TableName() string {
	return "invoice_product"
}

// Store an invoice product in the database
func (i *InvoiceProduct) Store() error {
	if i.InvoiceID == "" {
		return ErrMissingInvoiceID
	}

	if i.ProductID == "" {
		return ErrMissingProductID
	}

	if i.Quantity == 0 {
		return ErrMissingQuantity
	}

	if i.Price == 0 {
		return ErrMissingPrice
	}

	return storage.DB.Create(i).Error
}

// List the stored products of the same invoice id
func List(invoiceID string) ([]*InvoiceProduct, error) {
	list := []*InvoiceProduct{}
	err := storage.DB.Where("invoice_id = ?", invoiceID).Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}
