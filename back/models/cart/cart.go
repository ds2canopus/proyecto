package cart

import (
	"errors"
	"univalle/desarrollo/proyecto/back/storage"
)

var (
	// ErrMissingUserID the user id is missing
	ErrMissingUserID = errors.New("missing user id")
	// ErrMissingProductID the user id is missing
	ErrMissingProductID = errors.New("missing product id")
	// ErrMissingQuantity the product quantity is missing
	ErrMissingQuantity = errors.New("missing product quantity")
	// ErrInvalidProductID the product quantity is missing
	ErrInvalidProductID = errors.New("invalid product id")
	// ErrInvalidQuantity the product quantity is missing
	ErrInvalidQuantity = errors.New("not enough quantity")
)

// Cart represents a user car
type Cart struct {
	storage.TimeTracker
	UserID    string `json:"user_id" gorm:"primary_key;column:user_id"`
	ProductID string `json:"product_id" gorm:"primary_key;column:product_id"`
	Quantity  int    `json:"quantity"`
}

// TableName in the database
func (c *Cart) TableName() string {
	return "cart"
}

// Store a cart product in the database
func (c *Cart) Store() error {
	if c.UserID == "" {
		return ErrMissingUserID
	}

	if c.ProductID == "" {
		return ErrMissingProductID
	}

	if c.Quantity < 1 {
		return ErrMissingQuantity
	}

	rows, err := storage.DB.Table("product").Select("product.quantity").Where("product.product_id = ?", c.ProductID).Rows()
	if err != nil {
		return err
	}

	ok := rows.Next()
	if !ok {
		return ErrInvalidProductID
	}

	quantity := 0
	err = rows.Scan(&quantity)
	if err != nil {
		return err
	}

	if c.Quantity > quantity {
		return ErrInvalidQuantity
	}

	err = Delete(c.UserID, c.ProductID)
	if err != nil {
		return nil
	}

	return storage.DB.Create(c).Error
}

// Delete an product
func Delete(userID, productID string) error {
	if userID == "" {
		return ErrMissingUserID
	}

	if productID == "" {
		return ErrMissingProductID
	}

	return storage.DB.Unscoped().Where("user_id = ? AND product_id = ?", userID, productID).Delete(&Cart{UserID: userID, ProductID: productID}).Error
}

// List the stored cart
func List(userID string) ([]*Cart, error) {
	list := []*Cart{}
	err := storage.DB.Where("user_id = ?", userID).Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Clear a cart
func Clear(userID string) error {
	if userID == "" {
		return ErrMissingUserID
	}

	return storage.DB.Unscoped().Where("user_id = ?", userID).Delete(&Cart{}).Error
}
