package subcategory

import (
	"errors"
	"univalle/desarrollo/proyecto/back/shared/random"
	"univalle/desarrollo/proyecto/back/storage"
)

const subcategoryIDPrefix = "SUB"

var (
	// ErrMissingID the subcategory id is missing
	ErrMissingID = errors.New("missing category id")
	// ErrMissingName the subcategory name is missing
	ErrMissingName = errors.New("missing category name")
	// ErrMissingImage the subcategory image is missing
	ErrMissingImage = errors.New("missing category image")
	// ErrMissingCategory the subcategory id is missing
	ErrMissingCategory = errors.New("missing category id")
)

// Subcategory handles a subcategory information
type Subcategory struct {
	storage.TimeTracker
	ID         string `json:"id" gorm:"primary_key;column:subcategory_id"`
	CategoryID string `json:"category_id" gorm:"column:category_id"`
	Name       string `json:"name"`
	Image      string `json:"image"`
}

// TableName in the database
func (s *Subcategory) TableName() string {
	return "subcategory"
}

// Store a subcategory in the database
func (s *Subcategory) Store() error {
	if s.Name == "" {
		return ErrMissingName
	}

	if s.Image == "" {
		return ErrMissingImage
	}

	if s.CategoryID == "" {
		return ErrMissingCategory
	}

	s.ID = random.GenerateID(subcategoryIDPrefix)

	return storage.DB.Create(s).Error
}

// List the stored subcategories from a category
func List(categoryID string) ([]*Subcategory, error) {
	list := []*Subcategory{}

	err := storage.DB.Where("category_id = ?", categoryID).Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Delete a subcategory
func (s *Subcategory) Delete() error {
	if s.ID == "" {
		return ErrMissingID
	}

	return storage.DB.Delete(s).Error
}

// Update a subcategory
func (s *Subcategory) Update(name, image, categoryID string) error {
	if s.ID == "" {
		return ErrMissingID
	}

	if name != "" {
		s.Name = name
	}

	if categoryID != "" {
		s.CategoryID = categoryID
	}

	if image != "" {
		s.Image = image
	}

	return storage.DB.Model(s).Updates(s).Error
}

// Load a subcategory info
func Load(id string) (*Subcategory, error) {
	if id == "" {
		return nil, ErrMissingID
	}

	subcategory := &Subcategory{}

	err := storage.DB.First(subcategory, "subcategory_id = ?", id).Error
	if err != nil {
		return nil, err
	}

	return subcategory, nil
}
