package product

import (
	"errors"

	"univalle/desarrollo/proyecto/back/shared/random"
	"univalle/desarrollo/proyecto/back/storage"
)

const productIDPrefix = "PRD"

var (
	// ErrMissingID the product id is missing
	ErrMissingID = errors.New("missing product id")
	// ErrMissingName the product name is missing
	ErrMissingName = errors.New("missing product name")
	// ErrMissingDescription the product description is missing
	ErrMissingDescription = errors.New("missing product description")
	// ErrMissingImage the product image is missing
	ErrMissingImage = errors.New("missing product image")
	// ErrMissingPrice the product price is missing
	ErrMissingPrice = errors.New("missing product price")
	// ErrMissingIVA the product iva is missing
	ErrMissingIVA = errors.New("missing product iva")
)

// Product is an product to the store
type Product struct {
	storage.TimeTracker
	ID            string  `json:"id" gorm:"primary_key;column:product_id"`
	SubcategoryID string  `json:"subcategory_id" gorm:"column:subcategory_id"`
	Name          string  `json:"name" gorm:"column:name"`
	Price         float64 `json:"price" gorm:"column:price"`
	Description   string  `json:"description" gorm:"column:description"`
	Quantity      int     `json:"quantity" gorm:"column:quantity"`
	Image         string  `json:"image" gorm:"column:image"`
	IVA           float64 `json:"iva"`
}

// TableName in the database
func (p *Product) TableName() string {
	return "product"
}

// Store a product in the database
func (p *Product) Store() error {
	if p.Price == 0 {
		return ErrMissingPrice
	}

	if p.IVA == 0 {
		return ErrMissingIVA
	}

	if p.Name == "" {
		return ErrMissingName
	}

	if p.Description == "" {
		return ErrMissingDescription
	}

	if p.Image == "" {
		return ErrMissingImage
	}

	p.ID = random.GenerateID(productIDPrefix)

	return storage.DB.Create(p).Error
}

// Update a product
func (p *Product) Update(subcategoryID, name, description, image string, price, iva float64, quantity int) error {
	fields := map[string]interface{}{}

	if subcategoryID != "" {
		fields["subcategory_id"] = subcategoryID
	}

	if name != "" {
		fields["name"] = name
	}

	if price != 0 {
		fields["price"] = price
	}

	if iva != 0 {
		fields["iva"] = price
	}

	if description != "" {
		fields["description"] = description
	}

	if image != "" {
		fields["image"] = image
	}

	if quantity >= 0 {
		fields["quantity"] = quantity
	}

	return storage.DB.Model(p).Updates(fields).Error
}

// Load a product info
func Load(id string) (*Product, error) {
	if id == "" {
		return nil, ErrMissingID
	}

	product := &Product{}

	db := storage.DB.First(product, "product_id = ?", id)
	if db.Error != nil {
		return nil, db.Error
	}

	return product, nil
}

// Delete an product
func Delete(id string) error {
	if id == "" {
		return ErrMissingID
	}

	return storage.DB.Where("product_id = ?", id).Delete(&Product{}).Error
}

// List the stored products of the same subcategory id
func List(subcategoryID string) ([]*Product, error) {
	list := []*Product{}
	err := storage.DB.Where("subcategory_id = ?", subcategoryID).Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Discount product
func (p *Product) Discount(newQuantity int) error {
	fields := map[string]interface{}{}

	if newQuantity >= 0 {
		fields["quantity"] = newQuantity
	}

	return storage.DB.Model(p).Updates(fields).Error
}
