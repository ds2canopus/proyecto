package card

import (
	"errors"
	shared "univalle/desarrollo/proyecto/back/shared/payment"
	"univalle/desarrollo/proyecto/back/storage"
)

const cardIDPrefix = "CRD"

var (
	// ErrMissingID the card id is missing
	ErrMissingID = errors.New("missing card id")
	// ErrMissingUserID the user id is missing
	ErrMissingUserID = errors.New("missing user id")
	// ErrMissingExpirationDate the expiration date is missing
	ErrMissingExpirationDate = errors.New("missing expiration date")
	// ErrMissingPin the pin is missing
	ErrMissingPin = errors.New("missing pin")
	// ErrMissingName the name is missing
	ErrMissingName = errors.New("missing name")
	// ErrMissingType the card type is missing
	ErrMissingType = errors.New("missing card type")
)

// Card represents a user card
type Card struct {
	storage.TimeTracker
	ID             string             `json:"card_id" gorm:"primary_key;column:card_id"`
	Type           shared.PaymentType `json:"type"`
	UserID         string             `json:"user_id"`
	ExpirationDate string             `json:"expiration_date"`
	Pin            int                `json:"pin"`
	Name           string             `json:"name"`
}

// TableName in the database
func (c *Card) TableName() string {
	return "card"
}

// Store a card in the database
func (c *Card) Store() error {
	if c.ID == "" {
		return ErrMissingID
	}

	if c.UserID == "" {
		return ErrMissingUserID
	}

	if c.Pin == 0 {
		return ErrMissingPin
	}

	if c.ExpirationDate == "" {
		return ErrMissingExpirationDate
	}

	if c.Name == "" {
		return ErrMissingName
	}

	if c.Type == "" {
		return ErrMissingType
	}

	return storage.DB.Create(c).Error
}

// List the stored cards from a user
func List(userID string) ([]*Card, error) {
	list := []*Card{}

	err := storage.DB.Where("user_id = ?", userID).Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Load a card info
func Load(id string) (*Card, error) {
	if id == "" {
		return nil, ErrMissingID
	}

	card := &Card{}

	err := storage.DB.First(card, "card_id = ?", id).Error
	if err != nil {
		return nil, err
	}

	return card, nil
}

// Delete a card
func (c *Card) Delete() error {
	if c.ID == "" {
		return ErrMissingID
	}

	return storage.DB.Delete(c).Error
}
