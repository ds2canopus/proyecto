package payment

import (
	"errors"

	shared "univalle/desarrollo/proyecto/back/shared/payment"
	"univalle/desarrollo/proyecto/back/storage"
)

const (
	paymentIDPrefix = "PAY"
	cashLabel       = "CASH"
)

var (
	// ErrMissingInvoiceID the invoice id is missing
	ErrMissingInvoiceID = errors.New("missing invoice id")
	// ErrUnknownType the payment type is unknown
	ErrUnknownType = errors.New("missing payment type")
	// ErrMissingPercentage the percentage is missing
	ErrMissingPercentage = errors.New("missing percentage")
)

// Payment represents a payment method
type Payment struct {
	storage.TimeTracker
	InvoiceID  string             `json:"invoice_id" gorm:"primary_key"`
	Type       shared.PaymentType `json:"type" gorm:"primary_key"`
	CardID     string             `json:"card_id" gorm:"primary_key"`
	Percentage float64            `json:"percentage"`
	Quota      int                `json:"quota"`
}

// TableName in the database
func (p *Payment) TableName() string {
	return "payment"
}

// Store a payment in the database
func (p *Payment) Store() error {
	if p.InvoiceID == "" {
		return ErrMissingInvoiceID
	}

	if p.Percentage == 0 {
		return ErrMissingPercentage
	}

	if !shared.PaymentExists(p.Type) {
		return ErrUnknownType
	}

	if p.Type == shared.PaymentTypeCash {
		p.CardID = cashLabel
		p.Quota = 0 // reset any value that may come
	}

	if p.Type == shared.PaymentTypeCard {
		p.Quota = 0 // reset any value that may come
	}

	return storage.DB.Create(p).Error
}
