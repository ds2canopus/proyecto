package discount

import (
	"errors"
	"univalle/desarrollo/proyecto/back/shared/random"
	"univalle/desarrollo/proyecto/back/storage"
)

const discountIDPrefix = "DSC"

var (
	// ErrMissingID the discount id is missing
	ErrMissingID = errors.New("missing discount id")
	// ErrMissingUserID the user id is missing
	ErrMissingUserID = errors.New("missing user email")
	// ErrMissingPercentage the discount percentage is missing
	ErrMissingPercentage = errors.New("missing discount percentage")
	// ErrMissingName the discount name is missing
	ErrMissingName = errors.New("missing name")
)

// Discount represents a store dicount
type Discount struct {
	storage.TimeTracker
	ID         string  `json:"discount_id" gorm:"primary_key;column:discount_id"`
	Name       string  `json:"name"`
	UserID     string  `json:"user_id"`
	Used       bool    `json:"used"`
	Percentage float64 `json:"percentage"`
}

// TableName in the database
func (d *Discount) TableName() string {
	return "discount"
}

// ListAll lists all discounts
func ListAll() ([]*Discount, error) {
	list := []*Discount{}

	err := storage.DB.Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

// ListByUser lists all discounts from a given user
func ListByUser(userID string) ([]*Discount, error) {
	list := []*Discount{}

	err := storage.DB.Where("user_id = ? and used = false", userID).Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

// GetDicountStatus gets a discount status from a specific user
func GetDicountStatus(userID string) ([]*Discount, error) {
	list := []*Discount{}

	err := storage.DB.Select("used").Where("user_id = ?", userID).Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Store a discount in the database
func (d *Discount) Store() error {
	if d.UserID == "" {
		return ErrMissingUserID
	}

	if d.Percentage == float64(0) {
		return ErrMissingPercentage
	}

	if d.Name == "" {
		return ErrMissingName
	}

	d.ID = random.GenerateID(discountIDPrefix)
	d.Used = false

	return storage.DB.Create(d).Error
}

// Load a specific discount
func Load(id string) (*Discount, error) {
	if id == "" {
		return nil, ErrMissingID
	}

	discount := &Discount{}

	err := storage.DB.First(discount, "id = ?", id).Error
	if err != nil {
		return nil, err
	}

	return discount, nil
}

// Delete a discount
func (d *Discount) Delete() error {
	if d.ID == "" {
		return ErrMissingID
	}

	return storage.DB.Delete(d).Error
}

// UpdateStatus a discount status
func (d *Discount) UpdateStatus() error {
	fields := map[string]interface{}{
		"used": true,
	}

	return storage.DB.Model(d).Updates(fields).Error
}
