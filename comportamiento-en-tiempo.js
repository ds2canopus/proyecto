import { group, sleep } from 'k6';
import http from 'k6/http';

// Version: 1.2
// Creator: Load Impact v4.0 - k6 JS Test Script Recorder

export let options = {
    stages: [
        {
            "duration": "1m0s",
            "target": 10
        },
        {
            "duration": "8m0s",
            "target": 10
        },
        {
            "duration": "1m0s",
            "target": 0
        }
    ],
    maxRedirects: 0,
    discardResponseBodies: true,
};

export default function() {

	group("page_0 - http://localhost:8081/#/login-client", function() {
		let req, res;
		req = [];
		res = http.batch(req);
		sleep(0.50);
	});
	group("page_1 - http://localhost:8081/", function() {
		let req, res;
		req = [{
			"method": "get",
			"url": "http://localhost:8081/",
			"params": {
				"headers": {
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/app.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "*/*"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/about.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/add-category.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/add-discount.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/add-products.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/add-subcategoty.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/admin-home.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/admin-profile.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/checkout.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/create-admin.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/history.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/invoice.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/login-admin.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/report.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/search-category.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/search-products.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/search-subcategoty.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/search-user.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/sign-up.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/subcategory-products.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/user-home.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/user-profile.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/vendors~add-category~add-products~add-subcategoty~search-category~search-products.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/vendors~report~sign-up.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/img/logo2.84a70d42.png",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/img/background_palm_tree.6fd55196.jpeg",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/sockjs-node/info?t=1568916592978",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "*/*"
				}
			}
		},{
			"method": "get",
			"url": "http://10.84.8.40:8081/sockjs-node/info?t=1568916592981",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "*/*"
				}
			}
		}];
		res = http.batch(req);
		sleep(1.13);
		req = [{
			"method": "get",
			"url": "http://localhost:8081/login-admin.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "*/*"
				}
			}
		}];
		res = http.batch(req);
		sleep(7.60);
		req = [{
			"method": "post",
			"url": "http://localhost:8080/login/user",
			"body": "{\"email\":\"dnavas@truora.com\",\"password\":\"123\"}",
			"params": {
				"headers": {
					"Accept": "application/json, text/plain, */*",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Sec-Fetch-Mode": "cors",
					"Content-Type": "application/x-www-form-urlencoded"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/user-home.js",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "*/*"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8080/category",
			"params": {
				"headers": {
					"Accept": "application/json, text/plain, */*",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Sec-Fetch-Mode": "cors"
				}
			}
		},{
			"method": "get",
			"url": "http://localhost:8081/12332",
			"params": {
				"headers": {
					"Sec-Fetch-Mode": "no-cors",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8"
				}
			}
		}];
		res = http.batch(req);
		// Random sleep between 5s and 10s
		sleep(Math.floor(Math.random()*5+5));
	});

}